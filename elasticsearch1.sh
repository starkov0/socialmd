#!/bin/bash

if [[ $HOSTNAME = 'darwin' ]]; then
	EXEC=/usr/share/elasticsearch/bin/elasticsearch
else
	EXEC=elasticsearch
fi;

$EXEC -f -D es.config=./config/elasticsearch.yml
