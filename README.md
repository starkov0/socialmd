# SOCIALMD #

PolyMD is a medical-computer science project containing SocialMD and StethoMD.
StethoMD develops a smartphone-connected stethoscope enhancing auscultation listening.
SocialMD develops a medical social network allowing physicians sharing patient related data with each other. Patient data contains auscultation sounds.
MobileMD is a multi-plateform smartphone App encrypting messages on the Smartphone.
For more information over PolyMD (french) :
http://www.tdg.ch/geneve/actu-genevoise/Un-etudiant-cree-un-stethoscope-connecte-au-telephone-portable/story/27942846

## SocialMD - SocialApp ##
SocialMD project contains SocialApp, the the social network App.
The project is written with Python 2.7 with the use of Django framework 1.7.7.

## Python-Django IDE ##
Best IDE to use for the project : 
PyCharm : https://www.jetbrains.com/pycharm/
Free for students and startups :)

## Dependencies ##

### Debian ###
apt-get install python-celery python-django-celery python-dev python-django
libgeos-dev redis-server openjdk-7-jre-headless liblog4j1.2-java

### Python ###
SocialMD uses third-party Django Apps, which may be installed with the use of Pip :
```pip install -r requirements.pip```

### Third-Party ###
SocialMD uses third-party projects, which have to be installed:

* Redis
* ElasticSearch

## Start Dependencies ##
When starting the project, 3 processes need to be started:

* Redis
* Celery
* ElasticSearch

3 files can be used to start these 3 processes:
```
#!python

sh redis.sh
sh celery.sh
sh elasticsearch1
```

## Start SocialApp ##
Starting the project may be done via:
```
#!python

python manage.py runserver
```

## Database reset ##
```
#!python

python manage.py reset_db
python manage.py makemigrations
python manage.py migrate
```

## ElasticSearch reset ##
```
#!python

python manage.py rebuild_index
```

## SocialMD Configuration ##
Project Configurations may be found in the *settings.py* file.
These settings are for the moment not verified and need to be updated.

## SocialMD Content ##

### *socialMD/* ###

* *media/* : contains user images and message files are saved.
* *static/* : contains templates used by the elastic search search engine.
* *celery.py* : initialising a the celery interface (not sure it's actually needed)
* *settings.py* : the project settings file
* *urls.py* : the API definition
* *wsgi.py* : (i don't know...)

### *socialApp/* ###

* *migrations/* : contains  database migrations
* *models/* : contains database models
* *search_indexes/* : contains search engine indexes
* *serializers/* : contains serializer rules
* *tests/* : contains tests
* *utils/* : contains a few helper and custom functions
* *views/* : contains the views -> Django project views contain most of projects business logic

### other files ###

* *celery.py* : celery starter file
* *elasticsearch1* : elasticsearch starter file
* *manage.py* : Django project main CLI
* *redis.py* : redis starter file
* *shell.py* : Django project shell starter (used to try the App functions)
* *show_db.sh* : creates *socialApp_models.pdf* file containing SocialApp database models graph

## API Permissions ##
* Non-authenticated user may read user basic information.
* Authenticated users may read other user basic information and user relationships.
* Authenticated users may read/write self-information.
* Authenticated users may read/write self-relationships.
* Authenticated users may read/write self-conversations.
* Conversation users may read/write conversation-messages.
* No user may delete a conversation or a message 

Conversation and Message can only be archived (no deletion).

## Security Protocol ##
MobileMD security is based on AES and RSA encryption algorithms.

### User ###
* As the user creates an account, he chooses a user_password and the SocialApp creates an app_password and gives it to the user.
* SocialApp creates a public_key and a private_key and encrypts one version of private_key with user_password and an other version of private_key with app_password. Encryption is performed with AES algorithm.
* SocialApp saves user_password hash, app_password hash and encrypted private_keys.

### Conversation ###
* For conversation creation, MobileMD gets user's encrypted private_key and decrypts it with user_password. MobileMD creates a conversation_key and encrypt it with it's private_key. MobileMD sends encrypted conversation_key to SocialApp.
* For adding a user in conversation, MobileMD gets user's encrypted private_key and decrypts it with user_password. MobileMD gets encrypted conversation_key and decrypts it with self private_key. MobileMD gets other_user public_key and encrypts conversation_key with the use of other_user public_key. MobileMD sends other_user encrypted conversation_key to SocialApp.
* For message creation, MobileMD gets encrypted private_key and encrypted conversation_key. Private_key is decrypted with user_password. Conversation_key is decrypted with private_key. Message is encrypted with conversation_key. MobileMD sends encrypted message to SocialApp.

## How to run tests ##
```
#!python

python manage.py test
```

## TODO ##
* Return correct datetime objects for user location.
