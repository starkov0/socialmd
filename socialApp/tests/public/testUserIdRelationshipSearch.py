__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestUserIdRelationshipSearch(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_user_id_relationship_search(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()
        c4 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        results = c3.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        results = c1.get('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 2)

        results = c1.get('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 2)

        results = c1.get('/user_id=1/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 2)

        results = c1.get('/user_id=1/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 2)

        results = c1.get('/user_id=2/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'is_self')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c1.get('/user_id=2/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/user_id=2/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'is_self')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c1.get('/user_id=2/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/user_id=3/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'is_self')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c1.get('/user_id=3/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/user_id=3/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'is_self')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c1.get('/user_id=3/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/user_id=4/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c1.get('/user_id=4/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c1.get('/user_id=4/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c1.get('/user_id=4/relationship/search/keywords=/longitude=1.23/latitude=1.21/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            if relationship_content['user']['id'] == 2:
                self.assertEqual(relationship_content['relationship_state'], 'is_self')
            elif relationship_content['user']['id'] == 3:
                self.assertEqual(relationship_content['relationship_state'], 'waiting_other')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 2)

        results = c2.get('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c2.get('/user_id=1/relationship/search/keywords=/longitude=1.1/latitude=2.2/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            if relationship_content['user']['id'] == 2:
                self.assertEqual(relationship_content['relationship_state'], 'is_self')
            elif relationship_content['user']['id'] == 3:
                self.assertEqual(relationship_content['relationship_state'], 'waiting_other')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 2)

        results = c2.get('/user_id=1/relationship/search/keywords=/longitude=1.1/latitude=2.2/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c2.get('/user_id=2/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=2/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=2/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=2/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=3/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=3/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=3/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=3/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/user_id=4/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=4/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=4/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=4/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            if relationship_content['user']['id'] == 2:
                self.assertEqual(relationship_content['relationship_state'], 'waiting_self')
            elif relationship_content['user']['id'] == 3:
                self.assertEqual(relationship_content['relationship_state'], 'is_self')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 2)

        results = c3.get('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/user_id=1/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [2, 3])
            if relationship_content['user']['id'] == 2:
                self.assertEqual(relationship_content['relationship_state'], 'waiting_self')
            elif relationship_content['user']['id'] == 3:
                self.assertEqual(relationship_content['relationship_state'], 'is_self')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 2)

        results = c3.get('/user_id=1/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(len(content['relationships']), 0)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/user_id=2/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=2/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=2/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=2/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=3/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=3/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertEqual(relationship_content['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=3/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=3/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for relationship_content in content['relationships']:
            self.assertTrue(relationship_content['user']['id'] in [1])
            self.assertEqual(relationship_content['relationship_state'], 'accepted')
            self.assertTrue(isinstance(relationship_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/user_id=4/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=4/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=4/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=4/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c4.get('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 403)
        self.assertEqual(content['detail'], 'Authentication credentials were not provided.')

        results = c4.get('/user_id=2/relationship/search/keywords=/longitude=/latitude=/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 403)
        self.assertEqual(content['detail'], 'Authentication credentials were not provided.')

        results = c4.get('/user_id=3/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 403)
        self.assertEqual(content['detail'], 'Authentication credentials were not provided.')

        results = c4.get('/user_id=4/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 403)
        self.assertEqual(content['detail'], 'Authentication credentials were not provided.')

        results = c3.get('/user_id=1/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=True/index_from=20/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')

        results = c3.get('/user_id=1/relationship/search/keywords=/longitude=2.2/latitude=1.1/is_mutual=Tr2ue/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'is_mutual not in [True, False]')