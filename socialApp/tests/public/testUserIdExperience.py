__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import date
from datetime import datetime
import haystack


class TestUserIdExperience(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_user_experience(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        title1 = 'title1'
        title2 = 'title2'
        body1 = 'body1'
        body2 = 'body2'
        begin1 = date(2010, 2, 11)
        begin2 = date(2011, 2, 11)
        end1 = date(2012, 12, 01)
        end2 = date(2013, 12, 01)

        results = c1.post('/experience/create/',
                         {'title': title1,
                          'body': body1,
                          'begin': begin1,
                          'end': end1})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/experience/create/',
                         {'title': title2,
                          'body': body2,
                          'begin': begin2,
                          'end': end2})
        self.assertEqual(results.status_code, 200)

        results = c2.get('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)

        results = c3.get('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)

        results = c2.get('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 1)
        self.assertEqual(content['experience']['title'], title1)
        self.assertEqual(content['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c3.get('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 1)
        self.assertEqual(content['experience']['title'], title1)
        self.assertEqual(content['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c2.get('/user_id=1/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title2)
        self.assertEqual(content['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end2)

        results = c3.get('/user_id=1/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title2)
        self.assertEqual(content['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end2)

        results = c2.get('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)
        self.assertEqual(content['experiences'][0]['experience']['id'], 2)
        self.assertEqual(content['experiences'][0]['experience']['title'], title2)
        self.assertEqual(content['experiences'][0]['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['end'], '%Y-%m-%d').date(), end2)
        self.assertEqual(content['experiences'][1]['experience']['id'], 1)
        self.assertEqual(content['experiences'][1]['experience']['title'], title1)
        self.assertEqual(content['experiences'][1]['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c3.get('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)
        self.assertEqual(content['experiences'][0]['experience']['id'], 2)
        self.assertEqual(content['experiences'][0]['experience']['title'], title2)
        self.assertEqual(content['experiences'][0]['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['end'], '%Y-%m-%d').date(), end2)
        self.assertEqual(content['experiences'][1]['experience']['id'], 1)
        self.assertEqual(content['experiences'][1]['experience']['title'], title1)
        self.assertEqual(content['experiences'][1]['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c1.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c2.get('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=1/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c1.post('/activate/password_hash/',
                         {'email': email1, 'password_hash': password_hash})
        self.assertEqual(results.status_code, 200)

        results = c2.get('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)

        results = c3.get('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)

        results = c2.get('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 1)
        self.assertEqual(content['experience']['title'], title1)
        self.assertEqual(content['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c3.get('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 1)
        self.assertEqual(content['experience']['title'], title1)
        self.assertEqual(content['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c2.get('/user_id=1/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title2)
        self.assertEqual(content['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end2)

        results = c3.get('/user_id=1/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title2)
        self.assertEqual(content['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end2)

        results = c2.get('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)
        self.assertEqual(content['experiences'][0]['experience']['id'], 2)
        self.assertEqual(content['experiences'][0]['experience']['title'], title2)
        self.assertEqual(content['experiences'][0]['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['end'], '%Y-%m-%d').date(), end2)
        self.assertEqual(content['experiences'][1]['experience']['id'], 1)
        self.assertEqual(content['experiences'][1]['experience']['title'], title1)
        self.assertEqual(content['experiences'][1]['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c3.get('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)
        self.assertEqual(content['experiences'][0]['experience']['id'], 2)
        self.assertEqual(content['experiences'][0]['experience']['title'], title2)
        self.assertEqual(content['experiences'][0]['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['end'], '%Y-%m-%d').date(), end2)
        self.assertEqual(content['experiences'][1]['experience']['id'], 1)
        self.assertEqual(content['experiences'][1]['experience']['title'], title1)
        self.assertEqual(content['experiences'][1]['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c3.get('/user_id=2/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Experience matching query does not exist.')