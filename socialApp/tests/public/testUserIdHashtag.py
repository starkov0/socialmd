__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import date
from datetime import datetime
import haystack


class TestUserIdHashtag(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_user_hashtag(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        tag1 = 'tag1'
        tag2 = 'tag2'

        results = c1.post('/hashtag/create/',
                          {'tag': tag1})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/hashtag/create/',
                          {'tag': tag2})
        self.assertEqual(results.status_code, 200)

        results = c2.get('/user_id=1/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)

        results = c3.get('/user_id=1/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)

        results = c2.get('/user_id=1/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 1)
        self.assertEqual(content['hashtag']['tag'], tag1)

        results = c3.get('/user_id=1/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 1)
        self.assertEqual(content['hashtag']['tag'], tag1)

        results = c2.get('/user_id=1/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 2)
        self.assertEqual(content['hashtag']['tag'], tag2)

        results = c3.get('/user_id=1/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 2)
        self.assertEqual(content['hashtag']['tag'], tag2)

        results = c2.get('/user_id=1/hashtag/search/keywords=tag/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)
        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)
        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c3.get('/user_id=1/hashtag/search/keywords=tag/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)
        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)
        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c1.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c2.get('/user_id=1/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=1/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=1/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.get('/user_id=1/hashtag/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c3.get('/user_id=1/hashtag/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c1.post('/activate/password_hash/',
                         {'email': email1, 'password_hash': password_hash})
        self.assertEqual(results.status_code, 200)

        results = c2.get('/user_id=1/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)

        results = c3.get('/user_id=1/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)

        results = c2.get('/user_id=1/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 1)
        self.assertEqual(content['hashtag']['tag'], tag1)

        results = c3.get('/user_id=1/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 1)
        self.assertEqual(content['hashtag']['tag'], tag1)

        results = c2.get('/user_id=1/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 2)
        self.assertEqual(content['hashtag']['tag'], tag2)

        results = c3.get('/user_id=1/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 2)
        self.assertEqual(content['hashtag']['tag'], tag2)

        results = c2.get('/user_id=1/hashtag/search/keywords=tag/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)
        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)
        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c3.get('/user_id=1/hashtag/search/keywords=tag/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)
        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)
        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c3.get('/user_id=2/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Hashtag matching query does not exist.')