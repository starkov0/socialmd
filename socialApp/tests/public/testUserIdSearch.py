__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import date
from datetime import datetime
import haystack


class TestUserIdSearch(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_user_id_search(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()
        c4 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        results = c1.get('/user/search/keywords=/longitude=/latitude=/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], 'is_self')
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], 'accepted')
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], 'waiting_other')
            self.assertEqual(user_content['distance'], None)
        self.assertEqual(content['users_count'], 3)

        results = c2.get('/user/search/keywords=/longitude=/latitude=/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], 'accepted')
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], 'is_self')
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], 'waiting_other')
            self.assertEqual(user_content['distance'], None)
        self.assertEqual(content['users_count'], 3)

        results = c3.get('/user/search/keywords=/longitude=/latitude=/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], 'waiting_self')
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], 'waiting_self')
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], 'is_self')
            self.assertEqual(user_content['distance'], None)
        self.assertEqual(content['users_count'], 3)

        results = c4.get('/user/search/keywords=/longitude=/latitude=/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], None)
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], None)
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], None)
            self.assertEqual(user_content['distance'], None)
        self.assertEqual(content['users_count'], 3)

        results = c1.get('/user/search/keywords=/longitude=3.1/latitude=12.1/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], 'is_self')
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], 'accepted')
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], 'waiting_other')
            self.assertTrue(isinstance(user_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['users_count'], 3)

        results = c2.get('/user/search/keywords=/longitude=3.1/latitude=12.1/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], 'accepted')
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], 'is_self')
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], 'waiting_other')
            self.assertTrue(isinstance(user_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['users_count'], 3)

        results = c3.get('/user/search/keywords=/longitude=3.1/latitude=12.1/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], 'waiting_self')
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], 'waiting_self')
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], 'is_self')
            self.assertTrue(isinstance(user_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['users_count'], 3)

        results = c4.get('/user/search/keywords=/longitude=3.1/latitude=12.1/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for user_content in content['users']:
            self.assertTrue(user_content['user']['id'] in [1, 2, 3])
            if user_content['user']['id'] == 1:
                self.assertEqual(user_content['relationship_state'], None)
            elif user_content['user']['id'] == 2:
                self.assertEqual(user_content['relationship_state'], None)
            elif user_content['user']['id'] == 3:
                self.assertEqual(user_content['relationship_state'], None)
            self.assertTrue(isinstance(user_content['distance'], (int, long, float, complex)))
        self.assertEqual(content['users_count'], 3)

        results = c4.get('/user/search/keywords=/longitude=3.1/latitude=12.1/index_from=20/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')