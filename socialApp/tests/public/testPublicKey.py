__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestUserIdPublicKey(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_user_id_public_key(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key1 = 'public_key1'
        public_key2 = 'public_key2'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key1,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key2,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.get('/user_id=1/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c1.get('/user_id=2/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c2.get('/user_id=1/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c2.get('/user_id=2/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c1.get('/user_id=1/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c1.get('/user_id=2/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c2.get('/user_id=1/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c2.get('/user_id=2/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c2.post('/relationship/accept/user_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationship_state'], 'accepted')

        results = c1.get('/user_id=1/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')

        results = c1.get('/user_id=2/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['public_key'], public_key2)

        results = c2.get('/user_id=1/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['public_key'], public_key1)

        results = c2.get('/user_id=2/public_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_self and user_other accepted relationship does not exist')
