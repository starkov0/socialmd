__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import date
from datetime import datetime
import haystack


class TestHashtagOnlySearch(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_hashag_only_search(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        tag1 = 'tag1'
        tag2 = 'tag2'

        results = c1.post('/hashtag/create/',
                          {'tag': tag1})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/hashtag/create/',
                          {'tag': tag2})
        self.assertEqual(results.status_code, 200)

        results = c1.get('/hashtag_only/search/keywords=tag/index_from=0/index_to=4/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)
        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)
        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c2.get('/hashtag_only/search/keywords=tag/index_from=0/index_to=4/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)
        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)
        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c3.get('/hashtag_only/search/keywords=tag/index_from=0/index_to=4/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)
        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)
        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c1.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c2.get('/hashtag_only/search/keywords=tag/index_from=0/index_to=4/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 0)
        self.assertEqual(len(content['hashtags']), 0)

        results = c3.get('/hashtag_only/search/keywords=tag/index_from=0/index_to=4/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 0)
        self.assertEqual(len(content['hashtags']), 0)
