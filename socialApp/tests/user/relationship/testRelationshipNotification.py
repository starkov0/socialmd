__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from socialApp.models.relationship import Relationship
import haystack
from socialApp.models.notification import Notification
from haystack.query import SearchQuerySet


class TestRelationNotification(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_relationship_notification(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        notification = SearchQuerySet().models(Notification).filter(user_notified_id=1)[0].object
        self.assertEqual(notification.user_notifying_id, 2)
        self.assertEqual(notification.user_notified_id, 1)
        self.assertTrue(hasattr(notification, 'relationshipnotification'))
        self.assertFalse(hasattr(notification, 'conversationnotification'))
        self.assertFalse(hasattr(notification, 'messagenotification'))
        relationshipnotification = notification.relationshipnotification
        self.assertEqual(relationshipnotification.state, Relationship.IS_ACCEPTED)
        self.assertEqual(relationshipnotification.relationship_id, 1)

        notification = SearchQuerySet().models(Notification).filter(user_notified_id=2)[0].object
        self.assertEqual(notification.user_notified_id, 2)
        self.assertEqual(notification.user_notifying_id, 1)
        self.assertTrue(hasattr(notification, 'relationshipnotification'))
        self.assertFalse(hasattr(notification, 'conversationnotification'))
        self.assertFalse(hasattr(notification, 'messagenotification'))
        relationshipnotification = notification.relationshipnotification
        self.assertEqual(relationshipnotification.state, Relationship.IS_WAITING)
        self.assertEqual(relationshipnotification.relationship_id, 1)

        notification = SearchQuerySet().models(Notification).filter(user_notified_id=3)[0].object
        self.assertEqual(notification.user_notified_id, 3)
        self.assertEqual(notification.user_notifying_id, 1)
        self.assertTrue(hasattr(notification, 'relationshipnotification'))
        self.assertFalse(hasattr(notification, 'conversationnotification'))
        self.assertFalse(hasattr(notification, 'messagenotification'))
        relationshipnotification = notification.relationshipnotification
        self.assertEqual(relationshipnotification.state, Relationship.IS_WAITING)
        self.assertEqual(relationshipnotification.relationship_id, 2)

        results = c3.delete('/relationship/user_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationship_state'], 'no_relationship')

        self.assertEqual(SearchQuerySet().models(Notification).filter(user_notified_id=3).count(), 0)
        self.assertEqual(SearchQuerySet().models(Notification).filter(user_notified_id=1).count(), 1)
        self.assertEqual(SearchQuerySet().models(Notification).filter(user_notified_id=2).count(), 1)

        results = c2.delete('/relationship/user_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationship_state'], 'no_relationship')

        self.assertEqual(SearchQuerySet().models(Notification).filter(user_notified_id=3).count(), 0)
        self.assertEqual(SearchQuerySet().models(Notification).filter(user_notified_id=1).count(), 0)
        self.assertEqual(SearchQuerySet().models(Notification).filter(user_notified_id=2).count(), 0)
