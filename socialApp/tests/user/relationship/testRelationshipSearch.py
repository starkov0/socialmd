__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestRelationSearch(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_relationship_search(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        results = c1.get('/relationship/search/state=accepted/keywords=test/longitude=3.3/latitude=1.2/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships'][0]['relationship_state'], 'accepted')
        self.assertEqual(content['relationships'][0]['user']['id'], 2)
        self.assertTrue(isinstance(content['relationships'][0]['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c1.get('/relationship/search/state=waiting_self/keywords=test/longitude=3.3/latitude=1.2/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships'], [])
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/relationship/search/state=waiting_other/keywords=test/longitude=3.3/latitude=1.2/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships'][0]['relationship_state'], 'waiting_other')
        self.assertEqual(content['relationships'][0]['user']['id'], 3)
        self.assertTrue(isinstance(content['relationships'][0]['distance'], (int, long, float, complex)))
        self.assertEqual(content['relationships_count'], 1)

        results = c1.get('/relationship/search/state=accepted/keywords=test/longitude=/latitude=/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships'][0]['relationship_state'], 'accepted')
        self.assertEqual(content['relationships'][0]['user']['id'], 2)
        self.assertEqual(content['relationships'][0]['distance'], None)
        self.assertEqual(content['relationships_count'], 1)

        results = c1.get('/relationship/search/state=allah/keywords=test/longitude=/latitude=/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'state not in [accepted, waiting_self, waiting_other]')

        results = c1.get('/relationship/search/state=accepted/keywords=test/longitude=/latitude=/index_from=2/index_to=0/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')