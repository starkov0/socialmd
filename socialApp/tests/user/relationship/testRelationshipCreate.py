__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from socialApp.models.relationship import Relationship
from socialApp.models.relationshipnotification import RelationshipNotification
import haystack


class TestRelationCreate(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_relationship_create(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.get('/relationship/count/state=accepted/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/relationship/count/state=waiting_self/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/relationship/count/state=waiting_other/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c2.get('/relationship/count/state=accepted/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c2.get('/relationship/count/state=waiting_self/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c2.get('/relationship/count/state=waiting_other/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/relationship/count/state=accepted/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/relationship/count/state=waiting_self/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/relationship/count/state=waiting_other/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.post('/relationship/create/user_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'user_id is self')

        results = c1.post('/relationship/create/user_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationship_state'], 'waiting_other')

        results = c1.post('/relationship/create/user_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'UNIQUE constraint failed: socialApp_relationship.user_high_id, socialApp_relationship.user_low_id')

        results = c1.post('/relationship/create/user_id=3/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationship_state'], 'waiting_other')

        results = c1.post('/relationship/create/user_id=3/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'UNIQUE constraint failed: socialApp_relationship.user_high_id, socialApp_relationship.user_low_id')

        relationships = Relationship.objects.all()
        relationshipnotifications = RelationshipNotification.objects.all()

        self.assertEqual(relationships.count(), 2)
        self.assertEqual(relationshipnotifications.count(), 2)

        self.assertEqual(relationships[0].user_asking_id, 1)
        self.assertEqual(relationships[0].user_answering_id, 2)
        self.assertEqual(relationships[0].user_low_id, 1)
        self.assertEqual(relationships[0].user_high_id, 2)
        self.assertEqual(relationships[0].state, Relationship.IS_WAITING)

        self.assertEqual(relationships[1].user_asking_id, 1)
        self.assertEqual(relationships[1].user_answering_id, 3)
        self.assertEqual(relationships[1].user_low_id, 1)
        self.assertEqual(relationships[1].user_high_id, 3)
        self.assertEqual(relationships[1].state, Relationship.IS_WAITING)

        self.assertEqual(relationshipnotifications[0].notification.user_notifying_id, 1)
        self.assertEqual(relationshipnotifications[0].notification.user_notified_id, 2)
        self.assertEqual(relationshipnotifications[0].notification.has_seen, False)
        self.assertEqual(relationshipnotifications[0].notification.has_checked, False)
        self.assertEqual(relationshipnotifications[0].relationship, relationships[0])
        self.assertEqual(relationshipnotifications[0].state, Relationship.IS_WAITING)

        self.assertEqual(relationshipnotifications[1].notification.user_notifying_id, 1)
        self.assertEqual(relationshipnotifications[1].notification.user_notified_id, 3)
        self.assertEqual(relationshipnotifications[1].notification.has_seen, False)
        self.assertEqual(relationshipnotifications[1].notification.has_checked, False)
        self.assertEqual(relationshipnotifications[1].relationship, relationships[1])
        self.assertEqual(relationshipnotifications[1].state, Relationship.IS_WAITING)

        results = c1.get('/relationship/count/state=accepted/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/relationship/count/state=waiting_self/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c1.get('/relationship/count/state=waiting_other/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 2)

        results = c2.get('/relationship/count/state=accepted/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c2.get('/relationship/count/state=waiting_self/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 1)

        results = c2.get('/relationship/count/state=waiting_other/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/relationship/count/state=accepted/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/relationship/count/state=waiting_self/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 1)

        results = c3.get('/relationship/count/state=waiting_other/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['relationships_count'], 0)

        results = c3.get('/relationship/count/state=waiting1_other/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'state not in [accepted, waiting_self, waiting_other]')