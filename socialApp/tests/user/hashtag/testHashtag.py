__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestHashtag(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_hashtag(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        tag1 = 'tag1'
        tag2 = 'tag2'
        tag3 = 'tag3'

        results = c.get('/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 0)

        results = c.post('/hashtag/create/', {'tag': tag1})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 1)
        self.assertEqual(content['hashtag']['tag'], tag1)

        results = c.post('/hashtag/create/', {'tag': tag2})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 2)
        self.assertEqual(content['hashtag']['tag'], tag2)

        results = c.get('/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)

        results = c.get('/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 1)
        self.assertEqual(content['hashtag']['tag'], tag1)

        results = c.get('/hashtag/hashtag_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 2)
        self.assertEqual(content['hashtag']['tag'], tag2)

        results = c.get('/hashtag/search/keywords=tag/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 2)

        self.assertEqual(content['hashtags'][0]['hashtag']['id'], 1)
        self.assertEqual(content['hashtags'][0]['hashtag']['tag'], tag1)

        self.assertEqual(content['hashtags'][1]['hashtag']['id'], 2)
        self.assertEqual(content['hashtags'][1]['hashtag']['tag'], tag2)

        results = c.post('/hashtag/hashtag_id=2/', {'tag': tag3})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag']['id'], 2)
        self.assertEqual(content['hashtag']['tag'], tag3)

        results = c.delete('/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtag'], 'delete')

        results = c.delete('/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Hashtag matching query does not exist.')

        results = c.get('/hashtag/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['hashtags_count'], 1)

        results = c.get('/hashtag/hashtag_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Hashtag matching query does not exist.')

        results = c.post('/hashtag/hashtag_id=1/', {'tag': tag3})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Hashtag matching query does not exist.')