__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import date
from datetime import datetime
import haystack


class TestExperience(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_experience_error(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        title = 'title'
        body = 'body'
        begin = date(2013, 12, 01)
        end = date(2014, 2, 11)

        results = c.post('/experience/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field is required.'])
        self.assertEqual(content['body'], ['This field is required.'])
        self.assertEqual(content['begin'], ['This field is required.'])

        results = c.post('/experience/create/',
                         {'title': '',
                          'body': '',
                          'begin': '',
                          'end': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field may not be blank.'])
        self.assertEqual(content['body'], ['This field may not be blank.'])
        self.assertEqual(content['begin'], ['Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]]'])
        self.assertEqual(content['end'], ['Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]]'])

        results = c.post('/experience/create/',
                         {'title': title,
                          'body': body,
                          'begin': end,
                          'end': begin})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'end < begin')

        results = c.post('/experience/create/',
                         {'title': title,
                          'body': body,
                          'begin': begin,
                          'end': end})
        self.assertEqual(results.status_code, 200)

        results = c.post('/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field is required.'])
        self.assertEqual(content['body'], ['This field is required.'])
        self.assertEqual(content['begin'], ['This field is required.'])

        results = c.post('/experience/experience_id=1/',
                         {'title': '',
                          'body': '',
                          'begin': '',
                          'end': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field may not be blank.'])
        self.assertEqual(content['body'], ['This field may not be blank.'])
        self.assertEqual(content['begin'], ['Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]]'])
        self.assertEqual(content['end'], ['Date has wrong format. Use one of these formats instead: YYYY[-MM[-DD]]'])

        results = c.post('/experience/experience_id=1/',
                         {'title': title,
                          'body': body,
                          'begin': end,
                          'end': begin})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'end < begin')

        results = c.get('/experience/search/keywords=title,body/index_from=2/index_to=0/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')