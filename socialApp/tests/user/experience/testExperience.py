__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import date
from datetime import datetime
import haystack


class TestExperience(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_experience(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        title1 = 'title1'
        title2 = 'title2'
        title3 = 'title3'
        body1 = 'body1'
        body2 = 'body2'
        body3 = 'body3'
        begin1 = date(2010, 2, 11)
        begin2 = date(2011, 2, 11)
        end1 = date(2012, 12, 01)
        end2 = date(2013, 12, 01)

        results = c.get('/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 0)

        results = c.post('/experience/create/',
                         {'title': title1,
                          'body': body1,
                          'begin': begin1,
                          'end': end1})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 1)
        self.assertEqual(content['experience']['title'], title1)
        self.assertEqual(content['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c.post('/experience/create/',
                         {'title': title2,
                          'body': body2,
                          'begin': begin2,
                          'end': end2})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title2)
        self.assertEqual(content['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end2)

        results = c.get('/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)

        results = c.get('/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 1)
        self.assertEqual(content['experience']['title'], title1)
        self.assertEqual(content['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c.get('/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title2)
        self.assertEqual(content['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experience']['end'], '%Y-%m-%d').date(), end2)

        results = c.get('/experience/search/keywords=title,body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 2)

        self.assertEqual(content['experiences'][0]['experience']['id'], 2)
        self.assertEqual(content['experiences'][0]['experience']['title'], title2)
        self.assertEqual(content['experiences'][0]['experience']['body'], body2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(datetime.strptime(content['experiences'][0]['experience']['end'], '%Y-%m-%d').date(), end2)

        self.assertEqual(content['experiences'][1]['experience']['id'], 1)
        self.assertEqual(content['experiences'][1]['experience']['title'], title1)
        self.assertEqual(content['experiences'][1]['experience']['body'], body1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['begin'], '%Y-%m-%d').date(), begin1)
        self.assertEqual(datetime.strptime(content['experiences'][1]['experience']['end'], '%Y-%m-%d').date(), end1)

        results = c.post('/experience/experience_id=2/',
                         {'title': title3,
                          'body': body3,
                          'begin': begin2})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title3)
        self.assertEqual(content['experience']['body'], body3)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(content['experience']['end'], None)

        results = c.delete('/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience'], 'delete')

        results = c.delete('/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Experience matching query does not exist.')

        results = c.get('/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experiences_count'], 1)

        results = c.get('/experience/experience_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['experience']['id'], 2)
        self.assertEqual(content['experience']['title'], title3)
        self.assertEqual(content['experience']['body'], body3)
        self.assertEqual(datetime.strptime(content['experience']['begin'], '%Y-%m-%d').date(), begin2)
        self.assertEqual(content['experience']['end'], None)

        results = c.get('/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Experience matching query does not exist.')

        results = c.post('/experience/experience_id=1/',
                         {'title': title2,
                          'body': body2,
                          'begin': begin2,
                          'end': end2})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Experience matching query does not exist.')