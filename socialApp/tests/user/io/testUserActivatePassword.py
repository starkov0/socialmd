__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from socialApp.models.user import User
from haystack.query import SearchQuerySet
import haystack


class TestUserActivatePassword(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_create_user_deactivate_activate(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c.post('/activate/password_hash/',
                         {'email': email, 'password_hash': password_hash})
        content = json_to_data(results.content)
        user_content = content['user']
        self.assertEqual(results.status_code, 200)
        self.assertEqual(user_content['id'], 1)

        user = User.objects.get()
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.is_online, True)

        sqs = SearchQuerySet().models(User).filter(is_active=True)
        self.assertEqual(sqs.count(), 1)
        self.assertEqual(sqs[0].object, user)

    def test_create_user_deactivate_activate_error(self):
        email = 'test@mail.com'
        wrong_email_type = 'test'
        wrong_email = 'test@m.com'
        password_hash = 'password_hash'
        wrong_password_hash = 'password1'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c.post('/activate/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['This field is required.'])
        self.assertEqual(content['password_hash'], ['This field is required.'])

        results = c.post('/activate/password_hash/',
                         {'email': '', 'password_hash': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['This field may not be blank.'])
        self.assertEqual(content['password_hash'], ['This field may not be blank.'])

        results = c.post('/activate/password_hash/',
                         {'email': wrong_email_type, 'password_hash': password_hash})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['Enter a valid email address.'])

        results = c.post('/activate/password_hash/',
                         {'email': wrong_email, 'password_hash': password_hash})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c.post('/activate/password_hash/',
                         {'email': email, 'password_hash': wrong_password_hash})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        user = User.objects.get()
        self.assertEqual(user.is_active, False)
        self.assertEqual(user.is_online, False)

    def test_create_user_logout_activate(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/logout/')
        self.assertEqual(results.status_code, 200)

        results = c.post('/activate/password_hash/',
                         {'email': email, 'password_hash': password_hash})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        user = User.objects.get()
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.is_online, False)