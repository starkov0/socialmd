__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from socialApp.models.user import User
from haystack.query import SearchQuerySet
import haystack

class TestUserActivateSuperPassword(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_create_user_deactivate_activate(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        new_password_hash = 'new_password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        new_encrypted_user_key = 'new_encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c.post('/encrypted_super_user_key/',
                         {'email': email,
                          'super_password_hash': super_password_hash})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['encrypted_super_user_key'], User.objects.get().encrypted_super_user_key)

        results = c.post('/activate/super_password_hash/',
                         {'email': email,
                          'super_password_hash': super_password_hash,
                          'new_password_hash': new_password_hash,
                          'new_encrypted_user_key': new_encrypted_user_key})
        content = json_to_data(results.content)
        user_content = content['user']
        self.assertEqual(results.status_code, 200)
        self.assertEqual(user_content['id'], 1)

        user = User.objects.get()
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.is_online, True)
        self.assertTrue(user.check_password_hash(password_hash=new_password_hash))
        self.assertTrue(user.check_super_password_hash(super_password_hash=super_password_hash))

        sqs = SearchQuerySet().models(User).filter(is_active=True)
        self.assertEqual(sqs.count(), 1)
        self.assertEqual(sqs[0].object, user)

    def test_create_user_deactivate_activate_error(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        wrong_email = 'test1@mail.com'
        wrong_email_type = 'test'
        super_password_hash = 'super_password_hash'
        wrong_super_password_hash = 'wrong_super_password_hash'
        new_password_hash = 'new_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        new_encrypted_user_key = 'new_encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c.post('/activate/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['This field is required.'])
        self.assertEqual(content['super_password_hash'], ['This field is required.'])
        self.assertEqual(content['new_password_hash'], ['This field is required.'])
        self.assertEqual(content['new_encrypted_user_key'], ['This field is required.'])

        results = c.post('/activate/super_password_hash/',
                         {'email': '',
                          'super_password_hash': '',
                          'new_password_hash': '',
                          'new_encrypted_user_key': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['This field may not be blank.'])
        self.assertEqual(content['super_password_hash'], ['This field may not be blank.'])
        self.assertEqual(content['new_password_hash'], ['This field may not be blank.'])
        self.assertEqual(content['new_encrypted_user_key'], ['This field may not be blank.'])

        results = c.post('/activate/super_password_hash/',
                         {'email': wrong_email_type,
                          'super_password_hash': super_password_hash,
                          'new_password_hash': new_password_hash,
                          'new_encrypted_user_key': new_encrypted_user_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['Enter a valid email address.'])

        results = c.post('/activate/super_password_hash/',
                         {'email': wrong_email,
                          'super_password_hash': super_password_hash,
                          'new_password_hash': new_password_hash,
                          'new_encrypted_user_key': new_encrypted_user_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c.post('/activate/super_password_hash/',
                         {'email': email,
                          'super_password_hash': wrong_super_password_hash,
                          'new_password_hash': new_password_hash,
                          'new_encrypted_user_key': new_encrypted_user_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

    def test_create_user_logout_super_activate_super_login_double_logout(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        new_password_hash = 'new_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        new_encrypted_user_key = 'new_encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()

        results = c1.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/deactivate/')
        self.assertEqual(results.status_code, 200)

        results = c1.post('/activate/super_password_hash/',
                         {'email': email,
                          'super_password_hash': super_password_hash,
                          'new_password_hash': new_password_hash,
                          'new_encrypted_user_key': new_encrypted_user_key})
        content = json_to_data(results.content)
        user_content = content['user']
        self.assertEqual(results.status_code, 200)
        self.assertEqual(user_content['id'], 1)

        results = c2.post('/login/super_password_hash/',
                         {'email': email,
                          'super_password_hash': super_password_hash,
                          'new_password_hash': new_password_hash,
                          'new_encrypted_user_key': new_encrypted_user_key})
        content = json_to_data(results.content)
        user_content = content['user']
        self.assertEqual(results.status_code, 200)
        self.assertEqual(user_content['id'], 1)

        results = c1.post('/logout/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 403)
        self.assertEqual(content['detail'], 'Authentication credentials were not provided.')

        results = c2.post('/logout/')
        self.assertEqual(results.status_code, 200)