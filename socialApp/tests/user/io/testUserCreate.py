__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from datetime import datetime
from socialApp.utils.test_helper import json_to_data
from socialApp.models.user import User
from django.contrib.auth.hashers import (make_password, check_password)
from haystack.query import SearchQuerySet
import haystack

class TestUserCreate(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_create_user(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'
        longitude = 1.23
        latitude = 1.12

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': longitude,
                          'latitude': latitude})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['user']['id'], 1)
        self.assertEqual(content['user']['email'], email)
        self.assertEqual(content['user']['name'], '')
        self.assertEqual(content['user']['pseudo'], '')
        self.assertEqual(content['user']['location'], location)
        self.assertEqual(content['user']['title'], '')
        self.assertEqual(content['user']['abstract'], '')
        self.assertEqual(content['user']['image'], None)
        self.assertEqual(content['user']['is_verified'], False)
        self.assertEqual(content['user']['is_online'], True)

        user = User.objects.get()
        self.assertEqual(user.email, email)
        self.assertEqual(user.name, '')
        self.assertEqual(user.pseudo, '')
        self.assertEqual(user.location, location)
        self.assertEqual(user.longitude, longitude)
        self.assertEqual(user.latitude, latitude)
        self.assertEqual(user.title, '')
        self.assertEqual(user.abstract, '')
        self.assertNotEquals(user.image, None)
        self.assertEqual(user.is_authenticated(), True)
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.is_anonymous(), False)
        self.assertEqual(user.is_online, True)
        self.assertEqual(user.is_verified, False)
        self.assertEqual(type(user.last_login), datetime)
        self.assertEqual(type(user.created_at), datetime)

        self.assertNotEqual(user.password_hash, make_password(password=password_hash))
        self.assertTrue(check_password(password_hash, encoded=user.password_hash))
        self.assertNotEqual(user.super_password_hash, make_password(password=super_password_hash))
        self.assertTrue(check_password(super_password_hash, encoded=user.super_password_hash))

        self.assertEqual(user.encrypted_private_key, encrypted_private_key)
        self.assertEqual(user.encrypted_super_user_key, encrypted_super_user_key)
        self.assertEqual(user.encrypted_user_key, encrypted_user_key)

        sqs = SearchQuerySet().models(User).filter(is_active=True)
        self.assertEqual(sqs.count(), 1)
        self.assertEqual(sqs[0].object, user)

    def test_create_user_error(self):
        c = Client()
        results = c.post('/user/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['This field is required.'])
        self.assertEqual(content['password_hash'], ['This field is required.'])
        self.assertEqual(content['super_password_hash'], ['This field is required.'])
        self.assertEqual(content['encrypted_user_key'], ['This field is required.'])
        self.assertEqual(content['encrypted_super_user_key'], ['This field is required.'])
        self.assertEqual(content['encrypted_private_key'], ['This field is required.'])
        self.assertEqual(content['public_key'], ['This field is required.'])
        self.assertEqual(content['location'], ['This field is required.'])
        self.assertEqual(content['longitude'], ['This field is required.'])
        self.assertEqual(content['latitude'], ['This field is required.'])

        results = c.post('/user/create/',
                         {'email': '',
                          'password_hash': '',
                          'super_password_hash': '',
                          'encrypted_user_key': '',
                          'encrypted_super_user_key': '',
                          'encrypted_private_key': '',
                          'public_key': '',
                          'location': '',
                          'longitude': '',
                          'latitude': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['This field may not be blank.'])
        self.assertEqual(content['password_hash'], ['This field may not be blank.'])
        self.assertEqual(content['super_password_hash'], ['This field may not be blank.'])
        self.assertEqual(content['encrypted_user_key'], ['This field may not be blank.'])
        self.assertEqual(content['encrypted_super_user_key'], ['This field may not be blank.'])
        self.assertEqual(content['encrypted_private_key'], ['This field may not be blank.'])
        self.assertEqual(content['public_key'], ['This field may not be blank.'])
        self.assertEqual(content['location'], ['This field may not be blank.'])
        self.assertEqual(content['longitude'], ['A valid number is required.'])
        self.assertEqual(content['latitude'], ['A valid number is required.'])

        email = 'test.email'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'
        longitude = 1.23
        latitude = 1.12

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': longitude,
                          'latitude': latitude})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['email'], ['Enter a valid email address.'])

        self.assertEqual(User.objects.count(), 0)

    def test_create_user_same_email(self):
        email = 'test@asd.email'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'
        longitude = 1.23
        latitude = 1.12

        c1 = Client()
        c2 = Client()

        results = c1.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': longitude,
                          'latitude': latitude})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': longitude,
                          'latitude': latitude})
        self.assertEqual(results.status_code, 404)

        user = User.objects.get()
        self.assertEqual(user.is_active, True)
        self.assertEqual(user.is_online, True)