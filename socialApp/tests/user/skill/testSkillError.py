__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import datetime
import haystack


class TestSkill(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_skill_error(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        title2 = 'title2'
        body2 = 'body2'

        results = c.post('/skill/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field is required.'])
        self.assertEqual(content['body'], ['This field is required.'])

        results = c.post('/skill/create/',
                         {'title': '',
                          'body': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field may not be blank.'])
        self.assertEqual(content['body'], ['This field may not be blank.'])

        results = c.post('/skill/create/',
                         {'title': title2,
                          'body': body2})
        self.assertEqual(results.status_code, 200)

        results = c.post('/skill/skill_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field is required.'])
        self.assertEqual(content['body'], ['This field is required.'])

        results = c.post('/skill/skill_id=1/',
                         {'title': '',
                          'body': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['title'], ['This field may not be blank.'])
        self.assertEqual(content['body'], ['This field may not be blank.'])

        results = c.get('/skill/search/keywords=title,body/index_from=2/index_to=0/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')