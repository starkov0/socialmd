__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import datetime
import haystack


class TestSkill(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_skill_two_users(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        title1 = 'title1'
        title2 = 'title2'
        body1 = 'body1'
        body2 = 'body2'

        results = c1.post('/skill/create/', {'title': title1, 'body': body1})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/skill/create/', {'title': title2, 'body': body2})
        self.assertEqual(results.status_code, 200)

        results = c1.get('/skill/skill_id=1/')
        self.assertEqual(results.status_code, 200)

        results = c1.get('/skill/skill_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Skill matching query does not exist.')

        results = c2.get('/skill/skill_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c2.get('/skill/skill_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Skill matching query does not exist.')