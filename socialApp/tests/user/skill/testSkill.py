__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from datetime import datetime
import haystack


class TestSkill(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_skill(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        title1 = 'title1'
        title2 = 'title2'
        title3 = 'title3'
        body1 = 'body1'
        body2 = 'body2'
        body3 = 'body3'

        results = c.get('/skill/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skills_count'], 0)

        results = c.post('/skill/create/',
                         {'title': title1,
                          'body': body1})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skill']['id'], 1)
        self.assertEqual(content['skill']['title'], title1)
        self.assertEqual(content['skill']['body'], body1)

        results = c.post('/skill/create/',
                         {'title': title2,
                          'body': body2})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skill']['id'], 2)
        self.assertEqual(content['skill']['title'], title2)
        self.assertEqual(content['skill']['body'], body2)

        results = c.get('/skill/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skills_count'], 2)

        results = c.get('/skill/skill_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skill']['id'], 1)
        self.assertEqual(content['skill']['title'], title1)
        self.assertEqual(content['skill']['body'], body1)

        results = c.get('/skill/skill_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skill']['id'], 2)
        self.assertEqual(content['skill']['title'], title2)
        self.assertEqual(content['skill']['body'], body2)

        results = c.get('/skill/search/keywords=title,body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skills_count'], 2)

        self.assertEqual(content['skills'][0]['skill']['id'], 1)
        self.assertEqual(content['skills'][0]['skill']['title'], title1)
        self.assertEqual(content['skills'][0]['skill']['body'], body1)

        self.assertEqual(content['skills'][1]['skill']['id'], 2)
        self.assertEqual(content['skills'][1]['skill']['title'], title2)
        self.assertEqual(content['skills'][1]['skill']['body'], body2)

        results = c.post('/skill/skill_id=2/',
                         {'title': title3,
                          'body': body3})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skill']['id'], 2)
        self.assertEqual(content['skill']['title'], title3)
        self.assertEqual(content['skill']['body'], body3)

        results = c.delete('/skill/skill_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skill'], 'delete')

        results = c.delete('/skill/skill_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Skill matching query does not exist.')

        results = c.get('/skill/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skills_count'], 1)

        results = c.get('/skill/skill_id=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['skill']['id'], 2)
        self.assertEqual(content['skill']['title'], title3)
        self.assertEqual(content['skill']['body'], body3)

        results = c.get('/skill/skill_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Skill matching query does not exist.')

        results = c.post('/skill/skill_id=1/',
                         {'title': title3,
                          'body': body3})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Skill matching query does not exist.')