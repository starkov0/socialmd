__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
from socialApp.models.user import User
import haystack


class TestSuperPassword(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_super_password(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        user = User.objects.get()
        self.assertTrue(user.check_password_hash(password_hash=password_hash))
        self.assertTrue(user.check_super_password_hash(super_password_hash=super_password_hash))

        new_super_password_hash = 'new_super_password_hash'
        new_encrypted_super_user_key = 'new_encrypted_super_user_key'

        results = c.post('/super_password_hash/',
                         {'password_hash': password_hash,
                          'new_super_password_hash': new_super_password_hash,
                          'new_encrypted_super_user_key': new_encrypted_super_user_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['super_password_hash'], 'update')

        user = User.objects.get()
        self.assertTrue(user.check_password_hash(password_hash=password_hash))
        self.assertTrue(user.check_super_password_hash(super_password_hash=new_super_password_hash))

    def test_super_password_error(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['password_hash'], ['This field is required.'])
        self.assertEqual(content['new_super_password_hash'], ['This field is required.'])
        self.assertEqual(content['new_encrypted_super_user_key'], ['This field is required.'])

        results = c.post('/super_password_hash/',
                         {'password_hash': '',
                          'new_super_password_hash': '',
                          'new_encrypted_super_user_key': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['password_hash'], ['This field may not be blank.'])
        self.assertEqual(content['new_super_password_hash'], ['This field may not be blank.'])
        self.assertEqual(content['new_encrypted_super_user_key'], ['This field may not be blank.'])

        new_super_password_hash = 'new_super_password_hash'
        new_encrypted_super_user_key = 'new_encrypted_super_user_key'

        results = c.post('/super_password_hash/',
                         {'password_hash': password_hash+'12',
                          'new_super_password_hash': new_super_password_hash,
                          'new_encrypted_super_user_key': new_encrypted_super_user_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'wrong password')

    def test_password_double_login(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        new_super_password_hash1 = 'new_super_password_hash1'
        new_super_password_hash2 = 'new_super_password_hash2'
        new_encrypted_super_user_key1 = 'new_encrypted_super_user_key1'
        new_encrypted_super_user_key2 = 'new_encrypted_super_user_key2'

        c1 = Client()
        c2 = Client()

        results = c1.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/login/password_hash/',
                          {'email': email,
                           'password_hash': password_hash})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/super_password_hash/',
                          {'password_hash': password_hash,
                           'new_super_password_hash': new_super_password_hash1,
                           'new_encrypted_super_user_key': new_encrypted_super_user_key1})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['super_password_hash'], 'update')

        user = User.objects.get()
        self.assertTrue(user.check_password_hash(password_hash=password_hash))
        self.assertTrue(user.check_super_password_hash(super_password_hash=new_super_password_hash1))

        results = c2.post('/super_password_hash/',
                         {'password_hash': password_hash,
                          'new_super_password_hash': new_super_password_hash2,
                          'new_encrypted_super_user_key': new_encrypted_super_user_key2})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['super_password_hash'], 'update')

        user = User.objects.get()
        self.assertTrue(user.check_password_hash(password_hash=password_hash))
        self.assertTrue(user.check_super_password_hash(super_password_hash=new_super_password_hash2))