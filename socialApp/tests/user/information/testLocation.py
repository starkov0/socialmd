__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class Testlocation(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_location(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.get('/location/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['location'], location)
        self.assertEqual(content['longitude'], 1.123)
        self.assertEqual(content['latitude'], 1.23)

        location = 'Lausanne'

        results = c.post('/location/',
                         {'location': location,
                          'longitude': 1.54,
                          'latitude': 1.45})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['location'], 'Lausanne')

        results = c.get('/location/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['location'], 'Lausanne')
        self.assertEqual(content['longitude'], 1.54)
        self.assertEqual(content['latitude'], 1.45)

    def test_password_location_error(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/location/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['location'], ['This field is required.'])
        self.assertEqual(content['longitude'], ['This field is required.'])
        self.assertEqual(content['latitude'], ['This field is required.'])

        results = c.post('/location/',
                         {'location': '',
                          'longitude': '',
                          'latitude': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['location'], ['This field may not be blank.'])
        self.assertEqual(content['longitude'], ['A valid number is required.'])
        self.assertEqual(content['latitude'], ['A valid number is required.'])