__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestAbstract(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_abstract(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.get('/user/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['user']['id'], 1)
        self.assertEqual(content['user']['email'], email)
        self.assertEqual(content['user']['name'], '')
        self.assertEqual(content['user']['pseudo'], '')
        self.assertEqual(content['user']['location'], location)
        self.assertEqual(content['user']['longitude'], 1.123)
        self.assertEqual(content['user']['latitude'], 1.23)
        self.assertEqual(content['user']['title'], '')
        self.assertEqual(content['user']['abstract'], '')
        self.assertEqual(content['user']['image'], None)
        self.assertEqual(content['user']['is_verified'], False)
        self.assertEqual(content['user']['is_online'], True)
