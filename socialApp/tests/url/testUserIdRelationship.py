__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestUrlUserIdRelationship(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_user_id_relationship(self):
        email = 'test1@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()

        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        #
        #

        results = c.post('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'POST' not allowed.")

        results = c.patch('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/user_id=1/relationship/search/keywords=/longitude=/latitude=/is_mutual=False/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.post('/user_id=2/relationship/count/is_mutual=False/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'POST' not allowed.")

        results = c.patch('/user_id=2/relationship/count/is_mutual=False/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/user_id=2/relationship/count/is_mutual=False/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/user_id=2/relationship/count/is_mutual=False/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")