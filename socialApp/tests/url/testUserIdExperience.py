__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestUrlUserIdExperience(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_information(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        #
        #

        results = c2.post('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'POST' not allowed.")

        results = c2.patch('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c2.put('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c2.delete('/user_id=1/experience/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c2.post('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'POST' not allowed.")

        results = c2.patch('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c2.put('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c2.delete('/user_id=1/experience/experience_id=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c2.post('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'POST' not allowed.")

        results = c2.patch('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c2.put('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c2.delete('/user_id=1/experience/search/keywords=body/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")