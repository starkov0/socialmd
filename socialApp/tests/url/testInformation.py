__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestUrlInformation(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_information(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        #
        #

        results = c.patch('/abstract/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/abstract/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/abstract/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.patch('/email/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/email/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/email/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.patch('/email/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/email/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/email/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.patch('/location/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/location/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/location/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.patch('/name/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/name/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/name/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.patch('/title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.patch('/image/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/image/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        #
        #

        results = c.get('/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.get('/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.post('/user/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'POST' not allowed.")

        results = c.patch('/user/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/user/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/user/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")
