__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestUrlUser(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_user(self):

        c = Client()

        results = c.get('/user/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/user/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/user/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/user/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.get('/activate/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/activate/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/activate/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/activate/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.get('/activate/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/activate/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/activate/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/activate/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.get('/login/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/login/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/login/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/login/password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.get('/login/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/login/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/login/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/login/super_password_hash/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()
        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.get('/deactivate/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/deactivate/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/deactivate/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/deactivate/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")

        #
        #

        results = c.get('/logout/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'GET' not allowed.")

        results = c.patch('/logout/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PATCH' not allowed.")

        results = c.put('/logout/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'PUT' not allowed.")

        results = c.delete('/logout/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 405)
        self.assertEqual(content['detail'], "Method 'DELETE' not allowed.")