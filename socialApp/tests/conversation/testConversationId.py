__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestConversationId(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_conversation_id(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()

        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        encrypted_title = 'encrypted_title'
        encrypted_conversation_key = 'encrypted_conversation_key'

        results = c.post('/conversation/create/',
                         {'encrypted_title': encrypted_title,
                          'encrypted_conversation_key': encrypted_conversation_key})
        self.assertEqual(results.status_code, 200)

        results = c.get('/conversation/count/is_archived=False/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 1)

        results = c.get('/conversation/count/is_archived=True/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 0)

        results = c.get('/conversation/search/keywords=/is_archived=False/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 1)
        for conversation_content in content['conversations']:
            self.assertEqual(conversation_content['conversation']['encrypted_title'], encrypted_title)
            self.assertEqual(conversation_content['conversation']['id'], 1)
            self.assertEqual(conversation_content['encrypted_conversation_key'], encrypted_conversation_key)
            for conversationuser_content in conversation_content['conversationusers']:
                self.assertTrue(conversationuser_content['user']['id'] in [1])
                if conversationuser_content['user']['id'] == 1:
                    self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                    self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
                    self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)

        results = c.get('/conversation/search/keywords=/is_archived=True/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 0)
        self.assertEqual(len(content['conversations']), 0)

        results = c.get('/conversation_id=1/conversation/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversation']['encrypted_title'], encrypted_title)
        self.assertEqual(content['conversation']['id'], 1)
        self.assertEqual(content['encrypted_conversation_key'], encrypted_conversation_key)
        for conversationuser_content in content['conversationusers']:
            self.assertTrue(conversationuser_content['user']['id'] in [1])
            if conversationuser_content['user']['id'] == 1:
                self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)

        results = c.post('/conversation_id=1/archive/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['is_archived'], True)

        results = c.get('/conversation/count/is_archived=False/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 0)

        results = c.get('/conversation/count/is_archived=True/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 1)

        results = c.get('/conversation/search/keywords=/is_archived=False/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 0)
        self.assertEqual(len(content['conversations']), 0)

        results = c.get('/conversation/search/keywords=/is_archived=True/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 1)
        for conversation_content in content['conversations']:
            self.assertEqual(conversation_content['conversation']['encrypted_title'], encrypted_title)
            self.assertEqual(conversation_content['conversation']['id'], 1)
            self.assertEqual(conversation_content['encrypted_conversation_key'], encrypted_conversation_key)
            for conversationuser_content in conversation_content['conversationusers']:
                self.assertTrue(conversationuser_content['user']['id'] in [1])
                if conversationuser_content['user']['id'] == 1:
                    self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], True)
                    self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
                    self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)

        results = c.get('/conversation_id=1/conversation/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversation']['encrypted_title'], encrypted_title)
        self.assertEqual(content['conversation']['id'], 1)
        self.assertEqual(content['encrypted_conversation_key'], encrypted_conversation_key)
        for conversationuser_content in content['conversationusers']:
            self.assertTrue(conversationuser_content['user']['id'] in [1])
            if conversationuser_content['user']['id'] == 1:
                self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], True)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)

        results = c.post('/conversation_id=1/archive/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['is_archived'], False)

        results = c.get('/conversation/count/is_archived=False/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 1)

        results = c.get('/conversation/count/is_archived=True/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 0)

        results = c.get('/conversation/search/keywords=/is_archived=False/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 1)
        for conversation_content in content['conversations']:
            self.assertEqual(conversation_content['conversation']['encrypted_title'], encrypted_title)
            self.assertEqual(conversation_content['conversation']['id'], 1)
            self.assertEqual(conversation_content['encrypted_conversation_key'], encrypted_conversation_key)
            for conversationuser_content in conversation_content['conversationusers']:
                self.assertTrue(conversationuser_content['user']['id'] in [1])
                if conversationuser_content['user']['id'] == 1:
                    self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                    self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
                    self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)

        results = c.get('/conversation/search/keywords=/is_archived=True/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversations_count'], 0)
        self.assertEqual(len(content['conversations']), 0)

        results = c.get('/conversation_id=1/conversation/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversation']['encrypted_title'], encrypted_title)
        self.assertEqual(content['conversation']['id'], 1)
        self.assertEqual(content['encrypted_conversation_key'], encrypted_conversation_key)
        for conversationuser_content in content['conversationusers']:
            self.assertTrue(conversationuser_content['user']['id'] in [1])
            if conversationuser_content['user']['id'] == 1:
                self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)

        results = c.get('/conversation_id=1/encrypted_conversation_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['encrypted_conversation_key'], encrypted_conversation_key)

        results = c.get('/conversation_id=1/encrypted_title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['encrypted_title'], encrypted_title)

        results = c.post('/conversation_id=1/encrypted_title/', {'encrypted_title': encrypted_title+'123'})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['encrypted_title'], encrypted_title+'123')

        results = c.get('/conversation_id=1/encrypted_title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['encrypted_title'], encrypted_title+'123')