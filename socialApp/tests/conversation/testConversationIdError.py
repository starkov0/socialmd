__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestConversationIdError(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_conversation_id_error(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/conversation/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_title'], ['This field is required.'])
        self.assertEqual(content['encrypted_conversation_key'], ['This field is required.'])

        results = c1.post('/conversation/create/',
                         {'encrypted_title': '',
                          'encrypted_conversation_key': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_title'], ['This field may not be blank.'])
        self.assertEqual(content['encrypted_conversation_key'], ['This field may not be blank.'])

        results = c1.get('/conversation/count/is_archived=Fals1e/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'is_archived not in [True, False]')

        results = c1.get('/conversation/search/keywords=/is_archived=Fal1se/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'is_archived not in [True, False]')

        results = c1.get('/conversation/search/keywords=/is_archived=False/index_from=10/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')

        results = c1.get('/conversation_id=12/conversation/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'ConversationUser matching query does not exist.')

        results = c1.post('/conversation_id=10/archive/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'ConversationUser matching query does not exist.')

        results = c1.get('/conversation_id=10/encrypted_conversation_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'ConversationUser matching query does not exist.')

        results = c1.get('/conversation_id=10/encrypted_title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.post('/conversation_id=1/encrypted_title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_title'], ['This field is required.'])

        results = c1.post('/conversation_id=1/encrypted_title/',
                         {'encrypted_title': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_title'], ['This field may not be blank.'])

        results = c2.get('/conversation_id=1/conversation/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'ConversationUser matching query does not exist.')

        results = c2.post('/conversation_id=1/archive/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'ConversationUser matching query does not exist.')

        results = c2.get('/conversation_id=1/encrypted_conversation_key/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'ConversationUser matching query does not exist.')

        results = c2.get('/conversation_id=1/encrypted_title/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c2.post('/conversation_id=1/encrypted_title/',
                         {'encrypted_title': 'lala'})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')
