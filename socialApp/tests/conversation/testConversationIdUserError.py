__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestConversationIdUserError(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_conversation_id_user_error(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        encrypted_title = 'encrypted_title'
        encrypted_conversation_key = 'encrypted_conversation_key'

        results = c1.post('/conversation/create/',
                         {'encrypted_title': encrypted_title,
                          'encrypted_conversation_key': encrypted_conversation_key})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/conversation_id=1/user_id=2/add/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_conversation_key'], ['This field is required.'])

        results = c1.post('/conversation_id=1/user_id=2/add/',
                         {'encrypted_conversation_key': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_conversation_key'], ['This field may not be blank.'])

        results = c1.post('/conversation_id=2/user_id=2/add/',
                         {'encrypted_conversation_key': encrypted_conversation_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.post('/conversation_id=1/user_id=3/add/',
                         {'encrypted_conversation_key': encrypted_conversation_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'User matching query does not exist.')

        results = c2.post('/conversation_id=2/user_id=2/add/',
                         {'encrypted_conversation_key': encrypted_conversation_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c2.post('/conversation_id=1/user_id=1/add/',
                         {'encrypted_conversation_key': encrypted_conversation_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.get('/conversation_id=2/user_id=1/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.get('/conversation_id=1/user_id=2/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'ConversationUser matching query does not exist.')

        results = c2.get('/conversation_id=1/user_id=1/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c2.get('/conversation_id=1/user_id=2/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.get('/conversation_id=2/user/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c2.get('/conversation_id=1/user/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.get('/conversation_id=2/user/search/keywords=tes/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.get('/conversation_id=1/user/search/keywords=tes/index_from=10/index_to=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')

        results = c2.get('/conversation_id=2/user/search/keywords=tes/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c2.get('/conversation_id=1/user/search/keywords=tes/index_from=10/index_to=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')

        results = c1.get('/conversation_id=2/user_to_add/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c2.get('/conversation_id=1/user_to_add/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.get('/conversation_id=2/user_to_add/search/keywords=tes/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c1.get('/conversation_id=1/user_to_add/search/keywords=tes/index_from=10/index_to=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')

        results = c2.get('/conversation_id=2/user_to_add/search/keywords=tes/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'Conversation matching query does not exist.')

        results = c2.get('/conversation_id=1/user_to_add/search/keywords=tes/index_from=10/index_to=1/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')


