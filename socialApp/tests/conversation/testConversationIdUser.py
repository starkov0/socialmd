__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestConversationIdUser(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_conversation_id_user(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        results = c3.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        encrypted_title = 'encrypted_title'
        encrypted_conversation_key = 'encrypted_conversation_key'

        results = c1.post('/conversation/create/',
                         {'encrypted_title': encrypted_title,
                          'encrypted_conversation_key': encrypted_conversation_key})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/conversation_id=1/user_id=2/add/',
                         {'encrypted_conversation_key': encrypted_conversation_key})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['user']['id'], 2)
        self.assertEqual(content['conversationuser_state']['is_archived'], False)
        self.assertEqual(content['conversationuser_state']['has_seen'], False)

        results = c1.get('/conversation_id=1/user_id=1/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['user']['id'], 1)
        self.assertEqual(content['conversationuser_state']['is_archived'], False)
        self.assertEqual(content['conversationuser_state']['has_seen'], True)

        results = c1.get('/conversation_id=1/user_id=2/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['user']['id'], 2)
        self.assertEqual(content['conversationuser_state']['is_archived'], False)
        self.assertEqual(content['conversationuser_state']['has_seen'], False)

        results = c2.get('/conversation_id=1/user_id=1/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['user']['id'], 1)
        self.assertEqual(content['conversationuser_state']['is_archived'], False)
        self.assertEqual(content['conversationuser_state']['has_seen'], True)

        results = c2.get('/conversation_id=1/user_id=2/conversationuser/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['user']['id'], 2)
        self.assertEqual(content['conversationuser_state']['is_archived'], False)
        self.assertEqual(content['conversationuser_state']['has_seen'], False)

        results = c1.get('/conversation_id=1/user/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversationusers_count'], 2)

        results = c2.get('/conversation_id=1/user/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversationusers_count'], 2)

        results = c1.get('/conversation_id=1/user/search/keywords=tes/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversationusers_count'], 2)
        for conversationuser_content in content['conversationusers']:
            self.assertTrue(conversationuser_content['user']['id'] in [1, 2])
            if conversationuser_content['user']['id'] == 1:
                self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
            elif conversationuser_content['user']['id'] == 2:
                self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], False)

        results = c2.get('/conversation_id=1/user/search/keywords=test/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['conversationusers_count'], 2)
        for conversationuser_content in content['conversationusers']:
            self.assertTrue(conversationuser_content['user']['id'] in [1, 2])
            if conversationuser_content['user']['id'] == 1:
                self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], True)
            elif conversationuser_content['user']['id'] == 2:
                self.assertEqual(conversationuser_content['conversationuser_state']['is_archived'], False)
                self.assertEqual(conversationuser_content['conversationuser_state']['has_seen'], False)

        results = c1.get('/conversation_id=1/user_to_add/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['users_count'], 1)

        results = c2.get('/conversation_id=1/user_to_add/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['users_count'], 0)

        results = c1.get('/conversation_id=1/user_to_add/search/keywords=tes/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['users_count'], 1)
        self.assertEqual(content['users'][0]['user']['id'], 3)

        results = c2.get('/conversation_id=1/user_to_add/search/keywords=/index_from=0/index_to=10/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['users_count'], 0)
        self.assertEqual(len(content['users']), 0)