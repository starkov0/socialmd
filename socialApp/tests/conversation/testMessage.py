__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack
from datetime import datetime


class TestMessage(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_message(self):
        email1 = 'test1@mail.com'
        email2 = 'test2@mail.com'
        email3 = 'test3@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c1 = Client()
        c2 = Client()
        c3 = Client()

        results = c1.post('/user/create/',
                         {'email': email1,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c2.post('/user/create/',
                         {'email': email2,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c3.post('/user/create/',
                         {'email': email3,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=2/')
        self.assertEqual(results.status_code, 200)

        results = c1.post('/relationship/create/user_id=3/')
        self.assertEqual(results.status_code, 200)

        results = c2.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        results = c3.post('/relationship/accept/user_id=1/')
        self.assertEqual(results.status_code, 200)

        encrypted_title = 'encrypted_title'
        encrypted_conversation_key = 'encrypted_conversation_key'

        results = c1.post('/conversation/create/',
                         {'encrypted_title': encrypted_title,
                          'encrypted_conversation_key': encrypted_conversation_key})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/conversation_id=1/user_id=2/add/',
                         {'encrypted_conversation_key': encrypted_conversation_key})
        self.assertEqual(results.status_code, 200)

        results = c1.post('/conversation_id=1/user_id=3/add/',
                         {'encrypted_conversation_key': encrypted_conversation_key})
        self.assertEqual(results.status_code, 200)

        results = c1.get('/conversation_id=1/message/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['messages_count'], 0)

        results = c2.get('/conversation_id=1/message/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['messages_count'], 0)

        results = c3.get('/conversation_id=1/message/count/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        self.assertEqual(content['messages_count'], 0)

        text1 = 'text1'
        text2 = 'text2'

        results = c1.post('/conversation_id=1/message/create/',
                         {'1': text1,
                          '2': text2})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 200)
        for messagetext_content in content['messagetexts']:
            self.assertTrue(messagetext_content['position'] in [1, 2])
            if messagetext_content['position'] == 1:
                self.assertEqual(messagetext_content['encrypted_text'], text1)
            elif messagetext_content['position'] == 2:
                self.assertEqual(messagetext_content['encrypted_text'], text2)
        self.assertEqual(len(content['messagefiles']), 0)
        self.assertTrue(datetime.strptime(content['message_created_at'], '%Y-%m-%dT%H:%M:%S.%fZ').__class__ == datetime)
        self.assertEqual(content['user']['id'], 1)