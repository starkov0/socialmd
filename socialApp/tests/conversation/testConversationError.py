__author__ = 'pierrestarkov'
from django.test import TestCase
from django.test import Client
from socialApp.utils.test_helper import json_to_data
import haystack


class TestConversationError(TestCase):

    def setUp(self):
        haystack.connections['default'].get_backend().clear()

    def tearDown(self):
        haystack.connections['default'].get_backend().clear()

    def test_conversation_error(self):
        email = 'test@mail.com'
        password_hash = 'password_hash'
        super_password_hash = 'super_password_hash'
        encrypted_user_key = 'encrypted_user_key'
        encrypted_super_user_key = 'encrypted_super_user_key'
        encrypted_private_key = 'encrypted_private_key'
        public_key = 'public_key'
        location = 'Geneve'

        c = Client()

        results = c.post('/user/create/',
                         {'email': email,
                          'password_hash': password_hash,
                          'super_password_hash': super_password_hash,
                          'encrypted_user_key': encrypted_user_key,
                          'encrypted_super_user_key': encrypted_super_user_key,
                          'encrypted_private_key': encrypted_private_key,
                          'public_key': public_key,
                          'location': location,
                          'longitude': '1.123',
                          'latitude': '1.23'})
        self.assertEqual(results.status_code, 200)

        results = c.post('/conversation/create/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_title'], ['This field is required.'])
        self.assertEqual(content['encrypted_conversation_key'], ['This field is required.'])

        results = c.post('/conversation/create/',
                         {'encrypted_title': '',
                          'encrypted_conversation_key': ''})
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 400)
        self.assertEqual(content['encrypted_title'], ['This field may not be blank.'])
        self.assertEqual(content['encrypted_conversation_key'], ['This field may not be blank.'])

        results = c.get('/conversation/count/is_archived=Fal2se/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'is_archived not in [True, False]')

        results = c.get('/conversation/search/keywords=/is_archived=Fal2se/index_from=0/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'is_archived not in [True, False]')

        results = c.get('/conversation/search/keywords=/is_archived=False/index_from=10/index_to=2/')
        content = json_to_data(results.content)
        self.assertEqual(results.status_code, 404)
        self.assertEqual(content['exception'], 'indexes incorrect')
