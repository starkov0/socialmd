__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.experience import Experience
from celery_haystack.indexes import CelerySearchIndex

class ExperienceIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    begin = indexes.DateField(model_attr='begin')
    user_id = indexes.IntegerField()

    def get_model(self):
        return Experience

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_user_id(self, obj):
        return obj.user_id