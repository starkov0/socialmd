__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.user import User
from celery_haystack.indexes import CelerySearchIndex
from socialApp.models.relationship import Relationship


class UserIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    location = indexes.LocationField(model_attr='get_location')
    is_active = indexes.BooleanField(model_attr='is_active')
    accepted_user_id = indexes.MultiValueField()
    waiting_self_user_id = indexes.MultiValueField()
    waiting_other_user_id = indexes.MultiValueField()
    conversation_id = indexes.MultiValueField()
    conversation_not_archived_id = indexes.MultiValueField()
    short_description = indexes.EdgeNgramField()

    def get_model(self):
        return User

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_accepted_user_id(self, obj):
        results = []
        for relationship in obj.relationship_high.all():
            if relationship.state == Relationship.IS_ACCEPTED and relationship.user_low.is_active:
                results.append(relationship.user_low_id)
        for relationship in obj.relationship_low.all():
            if relationship.state == Relationship.IS_ACCEPTED and relationship.user_high.is_active:
                results.append(relationship.user_high_id)
        return results

    def prepare_waiting_self_user_id(self, obj):
        results = []
        for relationship in obj.relationship_asking.all():
            if relationship.state == Relationship.IS_WAITING and relationship.user_answering.is_active:
                results.append(relationship.user_answering_id)
        return results

    def prepare_waiting_other_user_id(self, obj):
        results = []
        for relationship in obj.relationship_answering.all():
            if relationship.state == Relationship.IS_WAITING and relationship.user_asking.is_active:
                results.append(relationship.user_asking_id)
        return results

    def prepare_conversation_id(self, obj):
        results = []
        for conversationuser in obj.conversationuser_set.all():
            results.append(conversationuser.conversation.id)
        return results

    def prepare_conversation_not_archived_id(self, obj):
        results = []
        for conversationuser in obj.conversationuser_set.all():
            if not conversationuser.is_archived:
                results.append(conversationuser.conversation.id)
        return results

    def prepare_short_description(self, obj):
        return obj.email + ' ' + obj.name + ' ' + obj.pseudo