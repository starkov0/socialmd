__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.notification import Notification
from celery_haystack.indexes import CelerySearchIndex


class NotificationIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    user_notified_id = indexes.IntegerField()
    created_at = indexes.DateTimeField(model_attr='created_at')
    has_seen = indexes.BooleanField(model_attr='has_seen')

    def get_model(self):
        return Notification

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_user_notified_id(self, obj):
        return obj.user_notified_id