__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.message import Message
from celery_haystack.indexes import CelerySearchIndex


class MessageIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    conversation_id = indexes.IntegerField()
    conversationuser_conversation_conversationuser_user_id = indexes.MultiValueField()
    created_at = indexes.DateTimeField(model_attr='created_at')
    last_update = indexes.DateTimeField()

    def get_model(self):
        return Message

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_conversation_id(self, obj):
        return obj.conversationuser.conversation.id

    def prepare_conversationuser_conversation_conversationuser_user_id(self, obj):
        results = []
        for conversationuser in obj.conversationuser.conversation.conversationuser_set.all():
            results.append(conversationuser.user.id)
        return results

    def prepare_last_update(self, obj):
        resutls = None
        for conversationuser in obj.conversationuser.conversation.conversationuser_set.all():
            for message in conversationuser.message_set.all():
                if resutls == None:
                    resutls = message.created_at
                elif resutls < message.created_at:
                    resutls = message.created_at
        if resutls == None:
            resutls = obj.conversationuser.conversation.created_at
        return resutls
