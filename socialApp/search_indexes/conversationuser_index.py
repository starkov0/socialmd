__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.conversationuser import ConversationUser
from celery_haystack.indexes import CelerySearchIndex


class ConversationUserIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    has_seen = indexes.BooleanField(model_attr='has_seen')
    is_archived = indexes.BooleanField()
    conversation_id = indexes.IntegerField()
    user_id = indexes.IntegerField()
    conversation_conversationuser_user_id = indexes.MultiValueField()
    conversation_conversationuser_user_text = indexes.EdgeNgramField()

    def get_model(self):
        return ConversationUser

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_conversation_id(self, obj):
        return obj.conversation.id

    def prepare_user_id(self, obj):
        return obj.user.id

    def prepare_is_archived(self, obj):
        return obj.is_archived

    def prepare_conversation_conversationuser_user_id(self, obj):
        results = []
        for conversationuser in obj.conversation.conversationuser_set.all():
            results.append(conversationuser.user.id)
        return results

    def prepare_conversation_conversationuser_user_text(self, obj):
        results = ''
        for conversationuser in obj.conversation.conversationuser_set.all():
            results += ' ' + conversationuser.user.email +\
                       ' ' + conversationuser.user.name + \
                       ' ' + conversationuser.user.pseudo
        return results.strip()