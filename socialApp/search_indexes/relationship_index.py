__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.relationship import Relationship
from celery_haystack.indexes import CelerySearchIndex


class RelationshipIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    state = indexes.CharField(model_attr='state')
    user_asking_id = indexes.IntegerField()
    user_answering_id = indexes.IntegerField()

    def get_model(self):
        return Relationship

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_user_asking_id(self, obj):
        return obj.user_asking_id

    def prepare_user_answering_id(self, obj):
        return obj.user_answering_id