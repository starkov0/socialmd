__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.hashtag import Hashtag
from celery_haystack.indexes import CelerySearchIndex


class HashtagIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    tag = indexes.CharField(model_attr='tag')
    user_id = indexes.IntegerField()
    is_active = indexes.BooleanField()

    def get_model(self):
        return Hashtag

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_user_id(self, obj):
        return obj.user_id

    def prepare_is_active(self, obj):
        return obj.user.is_active