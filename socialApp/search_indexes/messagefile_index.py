__author__ = 'pierrestarkov'
from haystack import indexes
from socialApp.models.messagefile import MessageFile
from celery_haystack.indexes import CelerySearchIndex


class MessageFileIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    conversation_id = indexes.IntegerField()
    message_conversationuser_conversation_conversationuser_user_id = indexes.MultiValueField()
    created_at = indexes.DateTimeField(model_attr='created_at')

    def get_model(self):
        return MessageFile

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_conversation_id(self, obj):
        return obj.message.conversationuser.conversation.id

    def prepare_message_conversationuser_conversation_conversationuser_user_id(self, obj):
        results = []
        for conversationuser in obj.message.conversationuser.conversation.conversationuser_set.all():
            results.append(conversationuser.user.id)
        return results