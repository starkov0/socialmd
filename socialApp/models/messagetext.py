__author__ = 'pierrestarkov'
from django.db import models


class MessageText(models.Model):
    message = models.ForeignKey('Message')
    position = models.IntegerField()
    encrypted_text = models.TextField()

    class Meta:
        app_label = 'socialApp'