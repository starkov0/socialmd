__author__ = 'pierrestarkov'
from django.db import models


class MessageNotification(models.Model):
    notification = models.OneToOneField('Notification')
    message = models.ForeignKey('Message')
    conversation = models.ForeignKey('Conversation')

    class Meta:
        app_label = 'socialApp'