__author__ = 'pierrestarkov'
from django.db import models


def dir_path(message_id):
    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    dir_path = ''
    counter = 0
    while message_id > 0:
        message_id, rest = divmod(message_id, len(alphabet))
        dir_path += alphabet[rest]
        counter += 1
        if counter > 3:
            dir_path += '/'
            counter = 0
    if dir_path[-1] is not '/':
        dir_path += '/'
    return dir_path


def file_name(messagefile_id):
    numerals = "0123456789"
    file_path = ''
    while messagefile_id > 0:
        messagefile_id, rest = divmod(messagefile_id, len(numerals))
        file_path += numerals[rest]
    return file_path


def upload_path_handler(instance, filename):
    return "message/" + dir_path(message_id=instance.message.id) + file_name(messagefile_id=instance.id)


class MessageFile(models.Model):
    message = models.ForeignKey('Message')
    position = models.IntegerField()
    encrypted_file = models.FileField(upload_to=upload_path_handler)

    class Meta:
        app_label = 'socialApp'