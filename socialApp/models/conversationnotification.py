__author__ = 'pierrestarkov'
from django.db import models


class ConversationNotification(models.Model):
    notification = models.OneToOneField('Notification')
    conversation = models.ForeignKey('Conversation')

    class Meta:
        app_label = 'socialApp'