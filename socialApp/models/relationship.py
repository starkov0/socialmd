__author__ = 'pierrestarkov'
from django.db import models


def order_users(user_1_id, user_2_id):
    if user_1_id < user_2_id:
        return user_1_id, user_2_id
    else:
        return user_2_id, user_1_id


class Relationship(models.Model):
    IS_WAITING,  IS_ACCEPTED = 'is_waiting', 'is_accepted'
    CHOICES = (
        (IS_WAITING, 'IS_WAITING'),
        (IS_ACCEPTED, 'IS_ACCEPTED'),
    )

    user_low = models.ForeignKey('User', related_name='relationship_low')
    user_high = models.ForeignKey('User', related_name='relationship_high')
    user_asking = models.ForeignKey('User', related_name='relationship_asking')
    user_answering = models.ForeignKey('User', related_name='relationship_answering')

    state = models.TextField(choices=CHOICES, default=IS_WAITING)

    class Meta:
        unique_together = ("user_high", "user_low")
        app_label = 'socialApp'