__author__ = 'pierrestarkov'
from django.db import models


class Conversation(models.Model):
    encrypted_title = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialApp'