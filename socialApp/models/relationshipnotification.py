__author__ = 'pierrestarkov'
from django.db import models
from socialApp.models.relationship import Relationship


class RelationshipNotification(models.Model):
    notification = models.OneToOneField('Notification')
    relationship = models.ForeignKey('Relationship')
    state = models.TextField(choices=Relationship.CHOICES, default=Relationship.IS_WAITING)

    class Meta:
        app_label = 'socialApp'