__author__ = 'pierrestarkov'
from django.db import models


class Message(models.Model):
    conversationuser = models.ForeignKey('ConversationUser')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialApp'