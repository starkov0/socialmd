__author__ = 'pierrestarkov'
from django.db import models


class Skill(models.Model):
    user = models.ForeignKey('User')
    title = models.TextField()
    body = models.TextField()

    class Meta:
        app_label = 'socialApp'