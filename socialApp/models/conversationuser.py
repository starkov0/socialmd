__author__ = 'pierrestarkov'
from django.db import models


class ConversationUser(models.Model):
    user = models.ForeignKey('User')
    conversation = models.ForeignKey('Conversation')

    encrypted_conversation_key = models.TextField()

    has_seen = models.BooleanField(default=False)
    is_archived = models.BooleanField(default=False)
    last_seen = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("user", "conversation")
        app_label = 'socialApp'