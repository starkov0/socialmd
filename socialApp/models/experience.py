__author__ = 'pierrestarkov'
from django.db import models


class Experience(models.Model):
    user = models.ForeignKey('User')
    title = models.TextField()
    body = models.TextField()
    begin = models.DateField()
    end = models.DateField(null=True)

    class Meta:
        app_label = 'socialApp'