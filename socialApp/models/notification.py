__author__ = 'pierrestarkov'
from django.db import models


class Notification(models.Model):
    user_notifying = models.ForeignKey('User', related_name='notification_notifying')
    user_notified = models.ForeignKey('User', related_name='notification_notified')
    has_seen = models.BooleanField(default=False)
    has_checked = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'socialApp'