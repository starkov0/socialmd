__author__ = 'pierrestarkov'
from django.db import models


class Hashtag(models.Model):
    tag = models.TextField()
    user = models.ForeignKey('User')

    class Meta:
        app_label = 'socialApp'