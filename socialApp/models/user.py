__author__ = 'pierrestarkov'
from django.utils.crypto import salted_hmac
from django.db import models
from django.contrib.auth.hashers import (make_password, check_password)
from haystack.utils.geo import Point
from datetime import date
from easy_thumbnails.fields import ThumbnailerImageField


def upload_image_handler(instance, filename):
    today = date.today()
    today.strftime("%Y/%m/%d")
    return "user/" + today.strftime("%Y/%m/%d") + "/{id}_{file}".format(id=instance.id, file=filename)


def upload_thumbnail_handler(instance, filename):
    today = date.today()
    today.strftime("%Y/%m/%d")
    return "user/" + today.strftime("%Y/%m/%d") + "/{id}_thumbnail_{file}".format(id=instance.id, file=filename)


class User(models.Model):
    email = models.EmailField(unique=True)

    name = models.TextField()
    pseudo = models.TextField()

    location = models.TextField()
    latitude = models.FloatField()
    longitude = models.FloatField()

    title = models.TextField()
    abstract = models.TextField()

    image = models.ImageField(upload_to=upload_image_handler)
    thumbnail = ThumbnailerImageField(upload_to=upload_thumbnail_handler, resize_source=dict(size=(100, 100),
                                                                                             sharpen=True))

    is_active = models.BooleanField(default=True)
    is_online = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)

    last_login = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    password_hash = models.TextField()
    super_password_hash = models.TextField()

    encrypted_user_key = models.TextField()
    encrypted_super_user_key = models.TextField()

    encrypted_private_key = models.TextField()
    public_key = models.TextField()

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'email'

    class Meta:
        app_label = 'socialApp'

############################################################# KEY SET
    def set_password_hash(self, password_hash):
        self.password_hash = make_password(password=password_hash)

    def set_super_password_hash(self, super_password_hash):
        self.super_password_hash = make_password(password=super_password_hash)

############################################################# PASSWORD
    def check_password_hash(self, password_hash):
        return check_password(password=password_hash, encoded=self.password_hash)

    def check_super_password_hash(self, super_password_hash):
        return check_password(password=super_password_hash, encoded=self.super_password_hash)

############################################################# DJANGO
    def get_session_auth_hash(self):
        key_salt = "django.contrib.auth.models.AbstractBaseUser.get_session_auth_hash"
        return salted_hmac(key_salt, self.password_hash).hexdigest()

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

############################################################# HAYSTACK DISTANCE ORDERING
    def get_location(self):
        return Point((self.longitude, self.latitude))