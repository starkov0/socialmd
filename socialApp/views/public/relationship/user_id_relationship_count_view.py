__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.user import User
from rest_framework import permissions
from haystack.query import SearchQuerySet
from socialApp.utils.request_helper import get_user_in_request
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def user_id_relationship_count_view(request, user_id, is_mutual):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check mutual
        if is_mutual == 'True':
            is_mutual = True
        elif is_mutual == 'False':
            is_mutual = False
        else:
            raise Exception('is_mutual not in [True, False]')

        ###################################################### check user exists
        user_other = User.objects.get(id=user_id, is_active=True)

        ###################################################### search
        if is_mutual:
            sqs = SearchQuerySet().models(User)\
                .filter(accepted_user_id=Exact(user_other.id))\
                .filter(accepted_user_id=Exact(user_self.id))
        else:
            sqs = SearchQuerySet().models(User).filter(accepted_user_id=Exact(user_other.id))

        ###################################################### results
        results = {'relationships_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)