__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from socialApp.utils.request_helper import get_user_in_request
from haystack.query import SearchQuerySet
from socialApp.models.user import User
from haystack.utils.geo import Point
from socialApp.serializers.user_serializer import UserIdSerializer
from socialApp.serializers.relationship_serializer import relationship_state_serializer
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def user_id_relationship_search_view(request, user_id, keywords, longitude, latitude, is_mutual, index_from, index_to):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check index
        index_from = int(index_from)
        index_to = int(index_to)
        if index_from > index_to or index_from < 0 or index_to < 0:
            raise Exception('indexes incorrect')

        ###################################################### check mutual
        if is_mutual == 'True':
            is_mutual = True
        elif is_mutual == 'False':
            is_mutual = False
        else:
            raise Exception('is_mutual not in [True, False]')

        ###################################################### check user exists
        user_other = User.objects.get(id=user_id, is_active=True)

        ###################################################### search
        sub_results = []
        if is_mutual:
            sqs = SearchQuerySet().models(User)\
                .filter(accepted_user_id=Exact(user_other.id))\
                .filter(accepted_user_id=Exact(user_self.id))
        else:
            sqs = SearchQuerySet().models(User).filter(accepted_user_id=Exact(user_other.id))

        if keywords:
            sqs = sqs.autocomplete(content=keywords.replace(',', ' ').strip())

        if longitude and latitude:
            longitude = float(longitude)
            latitude = float(latitude)
            sqs = sqs.distance('location', Point(longitude, latitude)).order_by('distance')

            for query in sqs[index_from:index_to]:
                sub_results.append({'user': UserIdSerializer(query.object).data,
                                    'distance': query.distance.m,
                                    'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                                        user_other_id=query.object.id)})

        else:
            for query in sqs[index_from:index_to]:
                sub_results.append({'user': UserIdSerializer(query.object).data,
                                    'distance': None,
                                    'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                                        user_other_id=query.object.id)})

        ###################################################### results
        results = {'relationships': sub_results, 'relationships_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)