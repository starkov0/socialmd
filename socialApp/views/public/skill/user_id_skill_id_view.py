__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.serializers.user_serializer import SkillIdSerializer
from socialApp.models.skill import Skill
from socialApp.models.user import User


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.AllowAny,))
def user_id_skill_id_view(request, user_id, skill_id):
    try:
        ###################################################### check index
        user = User.objects.get(id=user_id, is_active=True)
        skill = Skill.objects.get(id=skill_id, user_id=user.id)
        results = {'skill': SkillIdSerializer(skill).data}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)