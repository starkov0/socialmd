__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.serializers.user_serializer import SkillIdSerializer
from socialApp.models.skill import Skill
from haystack.query import SearchQuerySet
from haystack.inputs import Exact
from socialApp.models.user import User


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.AllowAny,))
def user_id_skill_search_view(request, user_id, keywords, index_from, index_to):
    try:
        ###################################################### check index
        index_from = int(index_from)
        index_to = int(index_to)
        if index_from > index_to or index_from < 0 or index_to < 0:
            raise Exception('indexes incorrect')

        ###################################################### search
        user = User.objects.get(id=user_id, is_active=True)
        sqs = SearchQuerySet().models(Skill).filter(user_id=Exact(user.id))
        if keywords:
            sqs = sqs.autocomplete(content=keywords.replace(',', ' ').strip())
        sqs = sqs.order_by('title')
        sqs_index = sqs[index_from:index_to]

        ###################################################### results
        skill_results = []
        for query in sqs_index:
            skill_results.append({'skill': SkillIdSerializer(query.object).data})
        results = {'skills': skill_results, 'skills_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)