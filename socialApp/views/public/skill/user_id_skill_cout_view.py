__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.skill import Skill
from haystack.query import SearchQuerySet
from haystack.inputs import Exact
from socialApp.models.user import User


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.AllowAny,))
def user_id_skill_count_view(request, user_id):
    try:
        ###################################################### results
        user = User.objects.get(id=user_id, is_active=True)
        sqs = SearchQuerySet().models(Skill).filter(user_id=Exact(user.id))
        results = {'skills_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)