__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.user import User
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point
from socialApp.serializers.user_serializer import UserIdSerializer
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.relationship_serializer import relationship_state_serializer


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.AllowAny,))
def user_search_view(request, keywords, longitude, latitude, index_from, index_to):
    try:
        ###################################################### check index
        index_from = int(index_from)
        index_to = int(index_to)
        if index_from > index_to or index_from < 0 or index_to < 0:
            raise Exception('indexes incorrect')

        ###################################################### search
        sub_results = []
        sqs = SearchQuerySet().models(User).filter(is_active=True)
        if keywords:
            sqs = sqs.autocomplete(content=keywords.replace(',', ' ').strip())

        if longitude and latitude:
            longitude = float(longitude)
            latitude = float(latitude)
            sqs = sqs.distance('location', Point(longitude, latitude)).order_by('distance')

            if request.user.is_authenticated():
                user_self = get_user_in_request(request)
                for query in sqs[index_from:index_to]:
                    sub_results.append({'user': UserIdSerializer(query.object).data,
                                        'distance': query.distance.m,
                                        'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                                            user_other_id=query.object.id)})

            else:
                for query in sqs[index_from:index_to]:
                    sub_results.append({'user': UserIdSerializer(query.object).data,
                                        'distance': query.distance.m,
                                        'relationship_state': None})

        else:
            if request.user.is_authenticated():
                user_self = get_user_in_request(request)
                for query in sqs[index_from:index_to]:
                    sub_results.append({'user': UserIdSerializer(query.object).data,
                                        'distance': None,
                                        'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                                            user_other_id=query.object.id)})

            else:
                for query in sqs[index_from:index_to]:
                    sub_results.append({'user': UserIdSerializer(query.object).data,
                                        'distance': None,
                                        'relationship_state': None})

        ###################################################### results
        results = {'users': sub_results, 'users_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)