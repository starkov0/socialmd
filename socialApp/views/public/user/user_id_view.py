__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.user import User
from socialApp.serializers.user_serializer import UserIdSerializer
from haystack.query import SearchQuerySet
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.relationship_serializer import relationship_state_serializer
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.AllowAny,))
def user_id_view(request, user_id):
    try:
        ###################################################### check user exists
        sqs = SearchQuerySet().models(User).filter(is_active=True, id=Exact(user_id))
        if sqs.count() == 0:
            raise Exception('User matching query does not exist.')

        if request.user.is_authenticated():
            user_self = get_user_in_request(request)
            sqs = sqs.distance('location', user_self.get_location())
            query = sqs[0]
            results = {'user': UserIdSerializer(query.object).data,
                       'distance': query.distance.m,
                       'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                           user_other_id=query.object.id)}
        else:
            query = sqs[0]
            results = {'user': UserIdSerializer(query.object).data,
                       'distance': None,
                       'relationship_state': None}

        ###################################################### results
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)