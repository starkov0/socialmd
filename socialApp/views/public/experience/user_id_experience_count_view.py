__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.experience import Experience
from haystack.query import SearchQuerySet
from socialApp.models.user import User
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.AllowAny,))
def user_id_experience_count_view(request, user_id):
    try:
        user = User.objects.get(id=user_id, is_active=True)

        ###################################################### results
        sqs = SearchQuerySet().models(Experience).filter(user_id=Exact(user.id))
        results = {'experiences_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
