__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.user import User
from socialApp.utils.request_helper import get_user_in_request
from haystack.query import SearchQuerySet
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def user_id_public_key_view(request, user_id):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check accepted relationship
        sqs = SearchQuerySet().models(User).filter(is_active=True,
                                                   id=Exact(user_id),
                                                   accepted_user_id=Exact(user_self.id))
        if sqs.count() == 0:
            raise Exception('user_self and user_other accepted relationship does not exist')

        ###################################################### results
        results = {'public_key': sqs[0].object.public_key}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)