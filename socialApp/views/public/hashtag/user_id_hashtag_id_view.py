__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.hashtag import Hashtag
from socialApp.serializers.user_serializer import HashtagIdSerializer
from socialApp.models.user import User


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.AllowAny,))
def user_id_hashtag_id_view(request, user_id, hashtag_id):
    try:
        ###################################################### results
        user = User.objects.get(id=user_id, is_active=True)
        hashtag = Hashtag.objects.get(id=hashtag_id, user_id=user.id)
        results = {'hashtag': HashtagIdSerializer(hashtag).data}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)