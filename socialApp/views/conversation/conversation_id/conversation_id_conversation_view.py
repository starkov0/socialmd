__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from socialApp.serializers.conversation_serializer import ConversationIdSerializer, ConversationStateSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from haystack.query import SearchQuerySet
from haystack.inputs import Exact
from socialApp.models.message import Message
from socialApp.serializers.conversation_serializer import MessageTextSerializer, MessageFileIdSerializer


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_id_conversation_view(request, conversation_id):
    user_self = get_user_in_request(request)

    try:
        ###################################################### conversationuser_self
        conversationuser_self = ConversationUser.objects.get(user=user_self, conversation_id=conversation_id)

        ###################################################### search conversationuser
        sqs_conversationuser = SearchQuerySet().models(ConversationUser)\
            .filter(conversation_id=Exact(conversation_id))\
            .filter(conversation_conversationuser_user_id=user_self.id)\
            .order_by('text')

        sub_results_conversationuser = []
        for query in sqs_conversationuser:
            sub_results_conversationuser\
                .append({'user': UserIdShortSerializer(query.object.user).data,
                         'conversationuser_state': ConversationStateSerializer(query.object).data})

        ###################################################### search message
        sqs_message = SearchQuerySet().models(Message)\
            .filter(conversation_id=conversation_id)\
            .filter(conversationuser_conversation_conversationuser_user_id=user_self.id)\
            .order_by('-last_update')

        sub_results_message = None
        if sqs_message.count():
            sub_results_messagetext = []
            sub_results_messagefile = []
            for messagetext in sqs_message[0].object.messagetext_set.all():
                sub_results_messagetext.append(MessageTextSerializer(messagetext).data)
            for messagefile in sqs_message[0].object.messagefile_set.all():
                sub_results_messagefile.append(MessageFileIdSerializer(messagefile).data)
            sub_results_message = {'messagetexts': sub_results_messagetext,
                                   'messagefiles': sub_results_messagefile,
                                   'message_created_at': sqs_message[0].object.created_at,
                                   'user': UserIdShortSerializer(user_self).data}

        ###################################################### results
        results = {'conversation': ConversationIdSerializer(conversationuser_self.conversation).data,
                   'conversationuser_state': ConversationStateSerializer(conversationuser_self).data,
                   'encrypted_conversation_key': conversationuser_self.encrypted_conversation_key,
                   'conversationusers': sub_results_conversationuser,
                   'last_message': sub_results_message}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)