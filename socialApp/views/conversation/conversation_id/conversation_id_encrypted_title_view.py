__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversation import Conversation
from socialApp.serializers.conversation_serializer import ConversationTitleSerializer
from django.db import transaction


@api_view(['GET', 'POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_id_encrypted_title_view(request, conversation_id):
    user_self = get_user_in_request(request)

    if request.method == 'GET':
        try:
            conversation = Conversation.objects.get(conversationuser__user=user_self, id=conversation_id)

            ###################################################### results
            results = ConversationTitleSerializer(conversation).data
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'POST':
        serializer = ConversationTitleSerializer(data=request.data)

        if serializer.is_valid():
            try:
                with transaction.atomic():
                    encrypted_title = serializer.validated_data['encrypted_title']

                    ###################################################### conversation set encrypted_title
                    conversation = Conversation.objects.get(id=conversation_id, conversationuser__user=user_self)
                    conversation.encrypted_title = encrypted_title
                    conversation.save()

                    ###################################################### results
                    results = ConversationTitleSerializer(conversation).data
                    return Response(results, status=status.HTTP_200_OK)

            except Exception as exception:
                return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)