__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from socialApp.serializers.conversation_serializer import ConversationKeySerializer


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_id_encrypted_conversation_key_view(request, conversation_id):
    user_self = get_user_in_request(request)

    try:
        conversationuser_self = ConversationUser.objects.get(user=user_self, conversation_id=conversation_id)

        ###################################################### results
        results = ConversationKeySerializer(conversationuser_self).data
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)