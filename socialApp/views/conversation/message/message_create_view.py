__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from django.db import transaction
from socialApp.models.message import Message
from socialApp.models.messagefile import MessageFile
from socialApp.models.messagetext import MessageText
from socialApp.models.messagenotification import MessageNotification
from socialApp.models.user import User
from socialApp.models.notification import Notification
from haystack.query import SearchQuerySet
from haystack.inputs import Exact
from socialApp.serializers.conversation_serializer import MessageTextSerializer, MessageFileIdSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from socialApp.utils.file_helper import remove_file


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def message_create_view(request, conversation_id):
    user_self = get_user_in_request(request)

    ###################################################### check request keys
    for key in request.data.keys():
        try:
            int(key)
        except Exception:
            message = 'key ', key, ' in ', request.data.keys(), ' is not an integer'
            return Response({'exception': message}, status=status.HTTP_400_BAD_REQUEST)

    try:
        with transaction.atomic():
            ###################################################### conversation set seen
            conversationuser_self = ConversationUser.objects.get(user=user_self, conversation_id=conversation_id)
            conversationuser_self.has_seen = True
            conversationuser_self.save()

            ###################################################### create message
            message = Message()
            message.conversationuser = conversationuser_self
            message.save()

            try:
                for key in request.data.keys():
                    if request.data[key].__class__.__name__ is 'InMemoryUploadedFile':
                        ###################################################### create messagefiles
                        messagefile = MessageFile()
                        messagefile.position = int(key)
                        messagefile.message = message
                        messagefile.encrypted_file = request.data[key]
                        messagefile.save()

                    else:
                        ###################################################### create messagetexts
                        messagetext = MessageText()
                        messagetext.position = int(key)
                        messagetext.message = message
                        messagetext.encrypted_text = request.data[key]
                        messagetext.save()

            except Exception as e:
                for messagefile in message.messagefile_set.all():
                    remove_file(messagefile.encrypted_file.path)
                    raise e

            ###################################################### create notification
            sqs = SearchQuerySet().models(User)\
                .filter(conversation_not_archived_id=Exact(conversation_id))\
                .exclude(user_id=user_self.id)

            for query in sqs:
                notification = Notification()
                notification.user_notifying = user_self
                notification.user_notified = query.object
                notification.save()

                messagenotification = MessageNotification()
                messagenotification.message = message
                messagenotification.conversation_id = conversation_id
                messagenotification.notification = notification
                messagenotification.save()

            ###################################################### conversationuser set unseen
            sqs = SearchQuerySet().models(ConversationUser)\
                .filter(conversation_id=conversation_id)\
                .exclude(user_id=user_self.id)

            for query in sqs:
                query.object.has_seen = False
                query.object.save()

            ###################################################### results
            sub_results_text = []
            sub_results_file = []
            for messagetext in message.messagetext_set.all():
                sub_results_text.append(MessageTextSerializer(messagetext).data)
            for messagefile in message.messagefile_set.all():
                sub_results_file.append(MessageFileIdSerializer(messagefile).data)
            results = {'messagetexts': sub_results_text,
                       'messagefiles': sub_results_file,
                       'message_created_at': message.created_at,
                       'user': UserIdShortSerializer(user_self).data}
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)