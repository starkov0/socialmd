__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.message import Message
from haystack.query import SearchQuerySet
from haystack.inputs import Exact
from socialApp.serializers.conversation_serializer import MessageTextSerializer, MessageFileIdSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from socialApp.models.conversationuser import ConversationUser
from django.db import transaction


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def message_id_view(request, conversation_id, message_id):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### conversation set seen
            conversationuser_self = ConversationUser.objects.get(user=user_self, conversation_id=conversation_id)
            conversationuser_self.has_seen = True
            conversationuser_self.save()

            ###################################################### search
            sqs = SearchQuerySet().models(Message)\
                .filter(id=Exact(message_id))\
                .filter(conversation_id=Exact(conversation_id))\

            if sqs.count() == 0:
                raise Exception('Message matching query does not exist.')

            ###################################################### results
            sub_results_text = []
            sub_results_file = []
            for messagetext in sqs[0].object.messagetext_set.all():
                sub_results_text.append({'messagetext': MessageTextSerializer(messagetext).data})
            for messagefile in sqs[0].object.messagefile_set.all():
                sub_results_file.append({'messagefile': MessageFileIdSerializer(messagefile).data})
            results = {'messagetexts': sub_results_text,
                       'messagefiles': sub_results_file,
                       'message_created_at': sqs[0].object.created_at,
                       'user': UserIdShortSerializer(sqs[0].object.conversationuser.user).data}
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response(exception, status=status.HTTP_200_OK)