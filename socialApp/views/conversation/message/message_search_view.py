__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.message import Message
from haystack.query import SearchQuerySet
from haystack.inputs import Exact
from socialApp.serializers.conversation_serializer import MessageTextSerializer, MessageFileIdSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from socialApp.models.conversationuser import ConversationUser
from django.db import transaction


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def message_search_view(request, conversation_id, index_from, index_to):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### check index
            index_from = int(index_from)
            index_to = int(index_to)
            if index_from > index_to or index_from < 0 or index_to < 0:
                raise Exception({'error': 'indexes incorrect'})

            ###################################################### conversation set seen
            conversationuser_self = ConversationUser.objects.get(user=user_self, conversation_id=conversation_id)
            conversationuser_self.has_seen = True
            conversationuser_self.save()

            ###################################################### search
            sqs = SearchQuerySet().models(Message)\
                .filter(conversation_id=Exact(conversation_id))\

            ###################################################### results
            sub_results = []
            for query in sqs[index_from:index_to]:
                sub_results_text = []
                sub_results_file = []
                for messagetext in query.object.messagetext_set.all():
                    sub_results_text.append({'messagetext': MessageTextSerializer(messagetext).data})
                for messagefile in query.object.messagefile_set.all():
                    sub_results_file.append({'messagefile': MessageFileIdSerializer(messagefile).data})
                sub_results.append({'messagetexts': sub_results_text,
                                    'messagefiles': sub_results_file,
                                    'message_created_at': query.created_at,
                                    'user': UserIdShortSerializer(query.conversationuser.user).data})
            results = {'messages': sub_results}
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response(exception, status=status.HTTP_200_OK)