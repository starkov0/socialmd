__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.messagefile import MessageFile
from haystack.query import SearchQuerySet
from haystack.inputs import Exact
from socialApp.serializers.conversation_serializer import EncryptedFileSerializer
from socialApp.models.conversationuser import ConversationUser
from django.db import transaction


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def messagefile_id_view(request, conversation_id, messagefile_id):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### conversation set seen
            conversationuser_self = ConversationUser.objects.get(user=user_self, conversation_id=conversation_id)
            conversationuser_self.has_seen = True
            conversationuser_self.save()

            ###################################################### search
            sqs = SearchQuerySet().models(MessageFile)\
                .filter(id=Exact(messagefile_id))\
                .filter(conversation_id=Exact(conversation_id))\

            if sqs.count() == 0:
                raise Exception('MessageFile matching query does not exist.')

            ###################################################### results
            results = EncryptedFileSerializer(sqs[0].object).data
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response(exception, status=status.HTTP_200_OK)