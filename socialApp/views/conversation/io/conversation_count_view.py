__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from haystack.query import SearchQuerySet
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_count_view(request, is_archived):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check is_archived
        if is_archived == 'True':
            is_archived = 'true'
        elif is_archived == 'False':
            is_archived = 'false'
        else:
            raise Exception('is_archived not in [True, False]')

        ###################################################### conversations
        sqs = SearchQuerySet().models(ConversationUser).filter(user_id=Exact(user_self.id),
                                                               is_archived=is_archived)

        ###################################################### results
        results = {'conversations_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)