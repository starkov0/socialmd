__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from haystack.query import SearchQuerySet
from socialApp.serializers.conversation_serializer import ConversationIdSerializer, ConversationStateSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from haystack.inputs import Exact
from socialApp.models.message import Message
from socialApp.serializers.conversation_serializer import MessageTextSerializer, MessageFileIdSerializer

@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_search_view(request, keywords, is_archived, index_from, index_to):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check index
        index_from = int(index_from)
        index_to = int(index_to)
        if index_from > index_to or index_from < 0 or index_to < 0:
            raise Exception('indexes incorrect')

        ###################################################### check is_archived
        if is_archived == 'True':
            is_archived = 'true'
        elif is_archived == 'False':
            is_archived = 'false'
        else:
            raise Exception('is_archived not in [True, False]')

        ###################################################### search conversationuser_self
        sqs_conversationuser_self = SearchQuerySet().models(ConversationUser)\
            .filter(user_id=Exact(user_self.id), is_archived=is_archived)

        if keywords:
            sqs_conversationuser_self = sqs_conversationuser_self\
                .autocomplete(conversation_conversationuser_user_text=keywords.replace(',', ' ').strip())

        sub_results_conversation = []
        for query_conversationuser_self in sqs_conversationuser_self[index_from:index_to]:

            ###################################################### search conversationuser_other
            sqs_conversationuser_other = SearchQuerySet().models(ConversationUser)\
                .filter(conversation_id=Exact(query_conversationuser_self.object.conversation.id))\
                .filter(conversation_conversationuser_user_id=user_self.id)\
                .order_by('text')

            sub_results_conversationuser = []
            for query_conversationuser_other in sqs_conversationuser_other:
                sub_results_conversationuser\
                    .append({'user': UserIdShortSerializer(query_conversationuser_other.object.user).data,
                             'conversationuser_state': ConversationStateSerializer(query_conversationuser_other.object).data})

            ###################################################### search message
            sqs_message = SearchQuerySet().models(Message)\
                .filter(conversation_id=query_conversationuser_self.object.conversation.id)\
                .filter(conversationuser_conversation_conversationuser_user_id=user_self.id)\
                .order_by('-last_update')

            sub_results_message = None
            if sqs_message.count():
                sub_results_messagetext = []
                sub_results_messagefile = []
                for messagetext in sqs_message[0].object.messagetext_set.all():
                    sub_results_messagetext.append(MessageTextSerializer(messagetext).data)
                for messagefile in sqs_message[0].object.messagefile_set.all():
                    sub_results_messagefile.append(MessageFileIdSerializer(messagefile).data)
                sub_results_message = {'messagetexts': sub_results_messagetext,
                                       'messagefiles': sub_results_messagefile,
                                       'message_created_at': sqs_message[0].object.created_at,
                                       'user': UserIdShortSerializer(user_self).data}

            sub_results_conversation\
                .append({'conversation': ConversationIdSerializer(query_conversationuser_self.object.conversation).data,
                         'conversationuser_state': ConversationStateSerializer(query_conversationuser_self.object).data,
                         'encrypted_conversation_key': query_conversationuser_self.object.encrypted_conversation_key,
                         'conversationusers': sub_results_conversationuser,
                         'last_message': sub_results_message})

        results = {'conversations': sub_results_conversation, 'conversations_count': sqs_conversationuser_self.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)