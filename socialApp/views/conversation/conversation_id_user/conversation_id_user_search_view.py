__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from haystack.query import SearchQuerySet
from socialApp.serializers.conversation_serializer import ConversationStateSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_id_user_search_view(request, conversation_id, keywords, index_from, index_to):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check index
        index_from = int(index_from)
        index_to = int(index_to)
        if index_from > index_to or index_from < 0 or index_to < 0:
            raise Exception('indexes incorrect')

        ###################################################### check user_self in conversation
        if not SearchQuerySet().models(ConversationUser)\
            .filter(conversation_id=Exact(conversation_id),
                    user_id=Exact(user_self.id)).count():
            raise Exception('Conversation matching query does not exist.')

        ###################################################### search
        sqs = SearchQuerySet().models(ConversationUser).filter(conversation_id=Exact(conversation_id))

        if keywords:
            sqs = sqs.autocomplete(conversation_conversationuser_user_text=keywords.replace(',', ' ').strip())

        ###################################################### results
        sub_results = []
        for query in sqs[index_from:index_to]:
            sub_results.append({'user': UserIdShortSerializer(query.object.user).data,
                                'conversationuser_state': ConversationStateSerializer(query.object).data})
        results = {'conversationusers': sub_results, 'conversationusers_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
