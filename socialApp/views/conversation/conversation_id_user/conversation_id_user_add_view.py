__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from django.db import transaction
from socialApp.models.user import User
from socialApp.models.conversationuser import ConversationUser
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.notification import Notification
from socialApp.models.conversationnotification import ConversationNotification
from socialApp.serializers.conversation_serializer import ConversationKeySerializer
from haystack.query import SearchQuerySet
from socialApp.serializers.conversation_serializer import ConversationStateSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from haystack.inputs import Exact


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_id_user_add_view(request, conversation_id, user_id):
    user_self = get_user_in_request(request)
    serializer = ConversationKeySerializer(data=request.data)

    if serializer.is_valid():
        try:
            with transaction.atomic():
                encrypted_conversation_key = serializer.validated_data['encrypted_conversation_key']

                ###################################################### check user_self in conversation
                if not SearchQuerySet().models(ConversationUser)\
                    .filter(conversation_id=Exact(conversation_id),
                            user_id=Exact(user_self.id)).count():
                    raise Exception('Conversation matching query does not exist.')

                ###################################################### check accepted relationship not in conversation
                sqs = SearchQuerySet().models(User)\
                    .filter(id=Exact(user_id),
                            is_active=True,
                            accepted_user_id=Exact(user_self.id))\
                    .exclude(conversation_id=Exact(conversation_id))
                if sqs.count() == 0:
                    raise Exception('User matching query does not exist.')
                if sqs.count() > 1:
                    raise Exception('SearchQuerySet returned multiple User objects.')

                conversationuser_other = ConversationUser()
                conversationuser_other.user = sqs[0].object
                conversationuser_other.conversation_id = conversation_id
                conversationuser_other.encrypted_conversation_key = encrypted_conversation_key
                conversationuser_other.save()

                ###################################################### create notifications
                notification = Notification()
                notification.user_notifying = user_self
                notification.user_notified = sqs[0].object
                notification.save()

                conversationnotification = ConversationNotification()
                conversationnotification.conversation_id = conversation_id
                conversationnotification.notification = notification
                conversationnotification.save()

                ###################################################### results
                results = {'user': UserIdShortSerializer(sqs[0].object).data,
                           'conversationuser_state': ConversationStateSerializer(conversationuser_other).data}
                return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

