__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from socialApp.serializers.conversation_serializer import ConversationStateSerializer
from socialApp.serializers.user_serializer import UserIdShortSerializer
from haystack.query import SearchQuerySet
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def conversation_id_user_id_conversationuser_view(request, conversation_id, user_id):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check user_self in conversation
        if not SearchQuerySet().models(ConversationUser)\
            .filter(conversation_id=Exact(conversation_id),
                    user_id=Exact(user_self.id)).count():
            raise Exception('Conversation matching query does not exist.')

        ###################################################### conversationuser
        sqs = SearchQuerySet().models(ConversationUser)\
            .filter(user_id=Exact(user_id),
                    conversation_id=Exact(conversation_id))
        if sqs.count() == 0:
            raise Exception('ConversationUser matching query does not exist.')

        ###################################################### results
        results = {'user': UserIdShortSerializer(sqs[0].object.user).data,
                   'conversationuser_state': ConversationStateSerializer(sqs[0].object).data}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
