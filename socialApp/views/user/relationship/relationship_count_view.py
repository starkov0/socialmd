__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.user import User
from rest_framework import permissions
from socialApp.utils.request_helper import get_user_in_request
from haystack.query import SearchQuerySet
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def relationship_count_view(request, state):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check who
        if state not in ['accepted', 'waiting_self', 'waiting_other']:
            raise Exception('state not in [accepted, waiting_self, waiting_other]')

        ###################################################### results
        if state == 'accepted':
            sqs = SearchQuerySet().models(User).filter(accepted_user_id=Exact(user_self.id))
        elif state == 'waiting_self':
            sqs = SearchQuerySet().models(User).filter(waiting_self_user_id=Exact(user_self.id))
        else:
            sqs = SearchQuerySet().models(User).filter(waiting_other_user_id=Exact(user_self.id))

        ###################################################### response
        results = {'relationships_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
