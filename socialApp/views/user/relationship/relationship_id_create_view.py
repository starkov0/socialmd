__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.relationship import Relationship, order_users
from socialApp.serializers.relationship_serializer import relationship_state_serializer
from django.db import transaction
from socialApp.models.user import User
from socialApp.models.relationshipnotification import RelationshipNotification
from socialApp.models.notification import Notification


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def relationship_id_create_view(request, user_id):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### check user_id
            if user_self.id == int(user_id):
                raise Exception('user_id is self')

            ###################################################### check user exists
            user_other = User.objects.get(id=user_id, is_active=True)

            ###################################################### create relationship
            user_low_id, user_high_id = order_users(user_1_id=user_self.id, user_2_id=user_other.id)
            relationship = Relationship()
            relationship.user_low_id = user_low_id
            relationship.user_high_id = user_high_id
            relationship.user_asking = user_self
            relationship.user_answering = user_other
            relationship.save()

            ###################################################### create notification
            notification = Notification()
            notification.user_notifying = user_self
            notification.user_notified = user_other
            notification.save()

            relationshipnotification = RelationshipNotification()
            relationshipnotification.relationship = relationship
            relationshipnotification.notification = notification
            relationshipnotification.save()

            ###################################################### results
            results = {'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                           user_other_id=user_other.id)}
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
