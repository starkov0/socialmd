__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.relationship import Relationship
from socialApp.serializers.relationship_serializer import relationship_state_serializer
from django.db import transaction
from socialApp.models.relationshipnotification import RelationshipNotification
from socialApp.models.notification import Notification
from socialApp.models.user import User


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def relationship_id_accept_view(request, user_id):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### check user exists
            user_other = User.objects.get(id=user_id, is_active=True)

            ###################################################### accept relationship
            relationship = Relationship.objects.get(user_asking=user_other,
                                                    user_answering=user_self,
                                                    state=Relationship.IS_WAITING)
            relationship.state = Relationship.IS_ACCEPTED
            relationship.save()

            ###################################################### create notification
            notification = Notification()
            notification.user_notifying = user_self
            notification.user_notified = user_other
            notification.save()

            relationshipnotification = RelationshipNotification()
            relationshipnotification.relationship = relationship
            relationshipnotification.notification = notification
            relationshipnotification.state = Relationship.IS_ACCEPTED
            relationshipnotification.save()

            ###################################################### results
            results = {'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                           user_other_id=user_other.id)}
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)