__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.user import User
from socialApp.serializers.relationship_serializer import relationship_state_serializer
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point
from socialApp.serializers.user_serializer import UserIdSerializer
from haystack.inputs import Exact


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def relationship_search_view(request, state, keywords, longitude, latitude, index_from, index_to):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check index
        index_from = int(index_from)
        index_to = int(index_to)
        if index_from > index_to or index_from < 0 or index_to < 0:
            raise Exception('indexes incorrect')

        ###################################################### check who
        if state not in ['accepted', 'waiting_self', 'waiting_other']:
            raise Exception('state not in [accepted, waiting_self, waiting_other]')

        sub_results = []

        ###################################################### search
        if state == 'accepted':
            sqs = SearchQuerySet().models(User).filter(accepted_user_id=Exact(user_self.id))
        elif state == 'waiting_self':
            sqs = SearchQuerySet().models(User).filter(waiting_self_user_id=Exact(user_self.id))
        else:
            sqs = SearchQuerySet().models(User).filter(waiting_other_user_id=Exact(user_self.id))

        if keywords:
            sqs = sqs.autocomplete(content=keywords.replace(',', ' ').strip())

        if longitude and latitude:
            longitude = float(longitude)
            latitude = float(latitude)
            sqs = sqs.distance('location', Point(longitude, latitude)).order_by('distance')

            user_self = get_user_in_request(request)
            for query in sqs[index_from:index_to]:
                sub_results.append({'user': UserIdSerializer(query.object).data,
                                    'distance': query.distance.m,
                                    'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                                        user_other_id=query.object.id)})

        else:
            user_self = get_user_in_request(request)
            for query in sqs[index_from:index_to]:
                sub_results.append({'user': UserIdSerializer(query.object).data,
                                    'distance': None,
                                    'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                                        user_other_id=query.object.id)})

        ###################################################### results
        results = {'relationships': sub_results, 'relationships_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)