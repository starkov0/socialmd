__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from socialApp.serializers.relationship_serializer import relationship_state_serializer
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.user import User
from django.db import transaction
from socialApp.models.relationship import Relationship
from django.db.models import Q
from socialApp.models.relationshipnotification import RelationshipNotification
from socialApp.models.notification import Notification


@api_view(['GET', 'DELETE'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def relationship_id_view(request, user_id):
    user_self = get_user_in_request(request)

    if request.method == 'GET':
        try:
            ###################################################### check user exists
            user_other = User.objects.get(id=user_id, is_active=True)

            ###################################################### results
            results = {'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                           user_other_id=user_other.id)}
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'DELETE':
        try:
            with transaction.atomic():
                ###################################################### check user exists
                user_other = User.objects.get(id=user_id, is_active=True)

                ###################################################### delete relationship
                Relationship.objects.get(
                    Q(user_low=user_self, user_high=user_other) |
                    Q(user_low=user_other, user_high=user_self)
                ).delete()

                Notification.objects.filter(
                    Q(user_notifying=user_self, user_notified=user_other) |
                    Q(user_notifying=user_other, user_notified=user_self)
                ).delete()

                ###################################################### results
                results = {'relationship_state': relationship_state_serializer(user_self_id=user_self.id,
                                                                               user_other_id=user_other.id)}
                return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
