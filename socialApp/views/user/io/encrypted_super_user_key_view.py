__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.serializers.user_serializer import EncryptedSuperUserKeySerializer, EmailSuperPasswordSerializer
from socialApp.models.user import User
from socialApp.utils import permissions


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsNotAuthenticated,))
def encrypted_super_user_key_view(request):
    serializer = EmailSuperPasswordSerializer(data=request.data)

    if serializer.is_valid():
        try:
            email = serializer.validated_data['email']
            super_password_hash = serializer.validated_data['super_password_hash']

            ###################################################### check super_password
            user_self = User.objects.get(email=email)
            if not user_self.check_super_password_hash(super_password_hash=super_password_hash):
                raise Exception('User matching query does not exist.')

            ###################################################### results
            user_self = User.objects.get(email=email)
            results = EncryptedSuperUserKeySerializer(user_self).data
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
