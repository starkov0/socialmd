__author__ = 'pierrestarkov'
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from django.contrib.auth import authenticate, login, logout
from socialApp.utils import permissions
from django.db import transaction
from socialApp.serializers.user_serializer import LoginPasswordSerializer, UserIdSerializer


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsNotAuthenticated, ))
def login_password_hash_view(request):
    serializer = LoginPasswordSerializer(data=request.data)

    if serializer.is_valid():
        try:
            with transaction.atomic():
                email = serializer.validated_data['email']
                password_hash = serializer.validated_data['password_hash']

                ###################################################### authenticate
                user_self = authenticate(username=email, password=password_hash)
                if not user_self:
                    raise Exception('User matching query does not exist.')

                ###################################################### login
                login(request=request, user=user_self)

                ###################################################### set is_online
                user_self.is_online = True
                user_self.save()

                ###################################################### results
                results = {'user': UserIdSerializer(user_self).data}
                return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            logout(request)
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)