__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from django.contrib.auth import authenticate, login, logout
from socialApp.models.user import User
from django.db import transaction
from socialApp.serializers.user_serializer import CreateUserSerializer
from socialApp.utils import permissions
from socialApp.serializers.user_serializer import UserIdSerializer


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsNotAuthenticated, ))
def user_create_view(request):
    serializer = CreateUserSerializer(data=request.data)

    if serializer.is_valid():
        try:
            with transaction.atomic():
                email = serializer.validated_data['email']
                password_hash = serializer.validated_data['password_hash']
                super_password_hash = serializer.validated_data['super_password_hash']
                encrypted_user_key = serializer.validated_data['encrypted_user_key']
                encrypted_super_user_key = serializer.validated_data['encrypted_super_user_key']
                encrypted_private_key = serializer.validated_data['encrypted_private_key']
                public_key = serializer.validated_data['public_key']
                location = serializer.validated_data['location']
                longitude = serializer.validated_data['longitude']
                latitude = serializer.validated_data['latitude']

                user_self = User()
                user_self.email = email
                user_self.set_password_hash(password_hash=password_hash)
                user_self.set_super_password_hash(super_password_hash=super_password_hash)
                user_self.encrypted_user_key = encrypted_user_key
                user_self.encrypted_super_user_key = encrypted_super_user_key
                user_self.encrypted_private_key = encrypted_private_key
                user_self.public_key = public_key
                user_self.location = location
                user_self.longitude = longitude
                user_self.latitude = latitude
                user_self.save()

                ###################################################### authenticate
                user_self = authenticate(username=email, password=password_hash)
                if not user_self:
                    raise Exception('User matching query does not exist.')

                ###################################################### login
                login(request=request, user=user_self)

                ###################################################### results
                results = {'user': UserIdSerializer(user_self).data}
                return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            logout(request)
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)