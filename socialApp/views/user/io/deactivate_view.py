__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from django.contrib.auth import logout
from django.db import transaction

@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def deactivate_view(request):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### deactivate user
            user_self.is_active = False
            user_self.is_online = False
            user_self.save()

            ###################################################### logout
            logout(request)

            ###################################################### results
            return Response({'user': 'deactivate'}, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
