__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from django.db import transaction
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.hashtag import Hashtag
from socialApp.serializers.user_serializer import HashtagIdSerializer, HashtagSerializer


@api_view(['GET', 'POST', 'DELETE'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def hashtag_id_view(request, hashtag_id):
    user_self = get_user_in_request(request)

    if request.method == 'GET':
        try:
            hashtag = Hashtag.objects.get(id=hashtag_id, user=user_self)

            ###################################################### results
            results = {'hashtag': HashtagIdSerializer(hashtag).data}
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'POST':
        serializer = HashtagSerializer(data=request.data)

        if serializer.is_valid():
            try:
                with transaction.atomic():
                    tag = serializer.validated_data['tag']

                    ###################################################### update hashtag
                    hashtag = Hashtag.objects.get(id=hashtag_id, user=user_self)
                    hashtag.tag = tag
                    hashtag.save()

                    ###################################################### results
                    results = {'hashtag': HashtagIdSerializer(hashtag).data}
                    return Response(results, status=status.HTTP_200_OK)

            except Exception as exception:
                return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            with transaction.atomic():
                hashtag = Hashtag.objects.get(id=hashtag_id, user=user_self)
                hashtag.delete()

                ###################################################### results
                results = {'hashtag': 'delete'}
                return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)
