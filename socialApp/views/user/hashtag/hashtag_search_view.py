__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.hashtag import Hashtag
from socialApp.serializers.user_serializer import HashtagIdSerializer
from socialApp.utils.request_helper import get_user_in_request
from haystack.query import SearchQuerySet
from haystack.inputs import Exact

@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def hashtag_search_view(request, keywords, index_from, index_to):
    user_self = get_user_in_request(request)

    try:
        ###################################################### check index
        index_from = int(index_from)
        index_to = int(index_to)
        if index_from > index_to or index_from < 0 or index_to < 0:
            raise Exception('indexes incorrect')

        ###################################################### search
        sqs = SearchQuerySet().models(Hashtag).filter(user_id=Exact(user_self.id))
        if keywords:
            sqs = sqs.autocomplete(content=keywords.replace(',', ' ').strip())
        sqs = sqs.order_by('tag')
        sqs_index = sqs[index_from:index_to]

        ###################################################### results
        hashtag_results = []
        for query in sqs_index:
            hashtag_results.append({'hashtag': HashtagIdSerializer(query.object).data})
        results = {'hashtags': hashtag_results, 'hashtags_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)