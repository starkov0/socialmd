__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.models.hashtag import Hashtag
from socialApp.serializers.user_serializer import HashtagIdSerializer
from socialApp.utils.request_helper import get_user_in_request
from haystack.query import SearchQuerySet
from haystack.inputs import Exact

@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def hashtag_count_view(request,):
    user_self = get_user_in_request(request)

    try:
        ###################################################### results
        sqs = SearchQuerySet().models(Hashtag).filter(user_id=Exact(user_self.id))
        results = {'hashtags_count': sqs.count()}
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)