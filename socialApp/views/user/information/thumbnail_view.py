__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.user_serializer import ThumbnailSerializer


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def thumbnail_view(request):
    user_self = get_user_in_request(request)

    try:
        ###################################################### results
        results = ThumbnailSerializer(user_self).data
        return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)