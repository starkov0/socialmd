__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.user_serializer import ImageSerializer
from django.db import transaction
from socialApp.utils.file_helper import remove_file


@api_view(['GET', 'POST', 'DELETE'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def image_view(request):
    user_self = get_user_in_request(request)

    if request.method == 'GET':
        try:
            ###################################################### results
            results = ImageSerializer(user_self).data
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'POST':
        serializer = ImageSerializer(data=request.data)

        if serializer.is_valid():
            try:
                with transaction.atomic():
                    ###################################################### delete image
                    user_self.image.delete()
                    user_self.thumbnail.delete()

                    ###################################################### upload image
                    user_self.image = serializer.validated_data['image']
                    user_self.thumbnail = serializer.validated_data['image']
                    user_self.save()

                    ###################################################### results
                    results = ImageSerializer(user_self).data
                    return Response(results, status=status.HTTP_200_OK)

            except Exception as exception:
                remove_file(user_self.image.path)
                return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        try:
            with transaction.atomic():
                ###################################################### delete image
                user_self.image.delete()
                user_self.thumbnail.delete()

                ###################################################### results
                results = ImageSerializer(user_self).data
                return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)