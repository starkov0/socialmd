__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from django.db import transaction
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.user_serializer import LocationSerializer


@api_view(['GET', 'POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def location_view(request):
    user_self = get_user_in_request(request)

    if request.method == 'GET':
        try:
            ###################################################### results
            results = LocationSerializer(user_self).data
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'POST':
        serializer = LocationSerializer(data=request.data)

        if serializer.is_valid():
            try:
                with transaction.atomic():
                    ###################################################### set location
                    location = serializer.validated_data['location']
                    longitude = serializer.validated_data['longitude']
                    latitude = serializer.validated_data['latitude']

                    user_self.location = location
                    user_self.longitude = longitude
                    user_self.latitude = latitude
                    user_self.save()

                    ###################################################### results
                    results = LocationSerializer(user_self).data
                    return Response(results, status=status.HTTP_200_OK)

            except Exception as exception:
                return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
