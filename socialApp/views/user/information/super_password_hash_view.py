__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from socialApp.utils.request_helper import get_user_in_request
from django.db import transaction
from socialApp.serializers.user_serializer import SuperPasswordSerializer
from django.contrib.auth import update_session_auth_hash


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def super_password_hash_view(request):
    user_self = get_user_in_request(request)
    serializer = SuperPasswordSerializer(data=request.data)

    if serializer.is_valid():
        try:
            with transaction.atomic():
                password_hash = serializer.validated_data['password_hash']
                new_super_password_hash = serializer.validated_data['new_super_password_hash']
                new_encrypted_super_user_key = serializer.validated_data['new_encrypted_super_user_key']

                 ###################################################### check password
                if not user_self.check_password_hash(password_hash=password_hash):
                    raise Exception('wrong password')

                ###################################################### set super_password
                user_self.set_super_password_hash(super_password_hash=new_super_password_hash)
                user_self.encrypted_super_user_key = new_encrypted_super_user_key
                user_self.save()

                ###################################################### update session
                update_session_auth_hash(request, user_self)

                ###################################################### results
                return Response({'super_password_hash': 'update'}, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)