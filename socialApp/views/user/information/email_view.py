__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from django.db import transaction
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.user_serializer import EmailPasswordSerializer, EmailSerializer


@api_view(['GET', 'POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def email_view(request):
    user_self = get_user_in_request(request)

    if request.method == 'GET':
        try:
            ###################################################### results
            results = EmailSerializer(user_self).data
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'POST':
        serializer = EmailPasswordSerializer(data=request.data)

        if serializer.is_valid():
            try:
                with transaction.atomic():
                    email = serializer.validated_data['email']
                    password_hash = serializer.validated_data['password_hash']

                    ###################################################### check password
                    if not user_self.check_password_hash(password_hash=password_hash):
                        raise Exception('wrong password_hash')

                    ###################################################### set email
                    user_self.email = email
                    user_self.save()

                    ###################################################### results
                    results = EmailSerializer(user_self).data
                    return Response(results, status=status.HTTP_200_OK)

            except Exception as exception:
                return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
