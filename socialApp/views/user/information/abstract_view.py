__author__ = 'pierrestarkov'
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from django.db import transaction
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.user_serializer import AbstractSerializer


@api_view(['GET', 'POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated,))
def abstract_view(request):
    user_self = get_user_in_request(request)

    if request.method == 'GET':
        try:
            ###################################################### results
            results = AbstractSerializer(user_self).data
            return Response(results, status=status.HTTP_200_OK)

        except Exception as exception:
            return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'POST':
        serializer = AbstractSerializer(data=request.data)

        if serializer.is_valid():
            try:
                with transaction.atomic():
                    ###################################################### set abstract
                    abstract = serializer.validated_data['abstract']
                    user_self.abstract = abstract
                    user_self.save()

                    ###################################################### results
                    results = AbstractSerializer(user_self).data
                    return Response(results, status=status.HTTP_200_OK)

            except Exception as exception:
                return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)