__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from django.db import transaction
from socialApp.models.notification import Notification
from socialApp.utils.request_helper import get_user_in_request


@api_view(['GET'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def notification_unseen_count_view(request):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            notifications = Notification.objects.filter(user_notified=user_self, has_seen=False)

            ###################################################### get notification
            results = {'notifications_count': notifications.count()}
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response(exception, status=status.HTTP_200_OK)