__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from django.db import transaction
from socialApp.models.notification import Notification
from socialApp.utils.request_helper import get_user_in_request
from socialApp.models.conversationuser import ConversationUser
from socialApp.serializers.user_serializer import UserIdShortSerializer
from socialApp.serializers.conversation_serializer import MessageTextSerializer, MessageFileIdSerializer


@api_view(['GET', 'POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def notification_check_view(request, notification_id):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### notifications set seen
            notifications = Notification.objects.filter(user_notified=user_self)
            for notification in notifications:
                notification.has_seen = True
                notification.save()

            ###################################################### search
            notification = Notification.objects.get(id=notification_id, user_notified=user_self)

            ###################################################### results
            if hasattr(notification, 'relationshipnotification'):
                results = {'notification_type': 'relationshipnotification',
                           'relationship_state': notification.relationshipnotification.state}

            elif hasattr(notification, 'conversationnotification'):
                results = {'notification_type': 'conversationnotification'}

            elif hasattr(notification, 'messagenotification'):
                message = notification.messagenotification.message
                sub_results_text = []
                sub_results_file = []
                for messagetext in message.messagetext_set.all():
                    sub_results_text.append(MessageTextSerializer(messagetext).data)
                for messagefile in message.messagefile_set.all():
                    sub_results_file.append(MessageFileIdSerializer(messagefile).data)
                sub_results_message = {'messagetexts': sub_results_text,
                                       'messagefiles': sub_results_file,
                                       'message_created_at': message.created_at,
                                       'user': UserIdShortSerializer(user_self).data}
                conversationuser_self = ConversationUser.objects.get(user=user_self,
                                                                     conversation_id=message.conversationuser.conversation_id)
                results = {'notification_type': 'messagenotification',
                           'message': sub_results_message,
                           'encrypted_conversation_key': conversationuser_self.encrypted_conversation_key}
            else:
                raise Exception('notification has no notification_type')

            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response({'exception': exception.message}, status=status.HTTP_404_NOT_FOUND)