__author__ = 'pierrestarkov'
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.cache import cache_control
from rest_framework import status
from rest_framework import permissions
from django.db import transaction
from socialApp.models.notification import Notification
from socialApp.utils.request_helper import get_user_in_request
from socialApp.serializers.notification_serializer import NotificationStateSerializer


@api_view(['POST'])
@cache_control(max_age=0)
@permission_classes((permissions.IsAuthenticated, ))
def notification_id_check_view(request, notification_id):
    user_self = get_user_in_request(request)

    try:
        with transaction.atomic():
            ###################################################### notifications set seen
            notifications = Notification.objects.filter(user_notified=user_self)
            for notification in notifications:
                notification.has_seen = True
                notification.save()

            ###################################################### notifications set checked
            notification = Notification.objects.get(id=notification_id, user_notified=user_self)
            notification.has_checked = True
            notification.save()

            ###################################################### results
            results = NotificationStateSerializer(notification).data
            return Response(results, status=status.HTTP_200_OK)

    except Exception as exception:
        return Response(exception, status=status.HTTP_200_OK)