__author__ = 'pierrestarkov'
from rest_framework import serializers


###################################################### CONVERSATION
class CreateConversationSerializer(serializers.Serializer):
    encrypted_title = serializers.CharField()
    encrypted_conversation_key = serializers.CharField()


class ConversationIdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    encrypted_title = serializers.CharField()


class ConversationTitleSerializer(serializers.Serializer):
    encrypted_title = serializers.CharField()


class ConversationKeySerializer(serializers.Serializer):
    encrypted_conversation_key = serializers.CharField()


class ConversationStateSerializer(serializers.Serializer):
    is_archived = serializers.BooleanField()
    has_seen = serializers.BooleanField()
    last_seen = serializers.DateTimeField()


class ConversationSeenSerializer(serializers.Serializer):
    has_seen = serializers.BooleanField()


class ConversationArchivedSerializer(serializers.Serializer):
    is_archived = serializers.BooleanField()


###################################################### MESSAGE
class MessageTextSerializer(serializers.Serializer):
    encrypted_text = serializers.CharField()
    position = serializers.IntegerField()


class MessageFileIdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    position = serializers.IntegerField()


class EncryptedFileSerializer(serializers.Serializer):
    encrypted_file = serializers.FileField()