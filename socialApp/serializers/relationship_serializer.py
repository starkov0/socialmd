__author__ = 'pierrestarkov'
from haystack.query import SearchQuerySet
from socialApp.models.user import User


def relationship_state_serializer(user_self_id, user_other_id):
    if user_self_id is user_other_id:
        return 'is_self'
    elif SearchQuerySet().models(User).filter(id=user_other_id, waiting_other_user_id=user_self_id):
        return 'waiting_other'
    elif SearchQuerySet().models(User).filter(id=user_other_id, waiting_self_user_id=user_self_id):
        return 'waiting_self'
    elif SearchQuerySet().models(User).filter(id=user_other_id, accepted_user_id=user_self_id):
        return 'accepted'
    else:
        return 'no_relationship'