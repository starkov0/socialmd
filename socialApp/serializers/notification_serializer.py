__author__ = 'pierrestarkov'
from rest_framework import serializers


###################################################### CONVERSATION
class NotificationIdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    has_seen = serializers.BooleanField()
    has_checked = serializers.BooleanField()
    created_at = serializers.DateTimeField()


class NotificationStateSerializer(serializers.Serializer):
    has_seen = serializers.BooleanField()
    has_checked = serializers.BooleanField()