from rest_framework import serializers


class AbstractSerializer(serializers.Serializer):
    abstract = serializers.CharField()


class EmailSerializer(serializers.Serializer):
    email = serializers.CharField()


class TitleSerializer(serializers.Serializer):
    title = serializers.CharField()


class NameSerializer(serializers.Serializer):
    name = serializers.CharField()


class LocationSerializer(serializers.Serializer):
    location = serializers.CharField()
    longitude = serializers.FloatField()
    latitude = serializers.FloatField()


class ImageSerializer(serializers.Serializer):
    image = serializers.ImageField()


class ThumbnailSerializer(serializers.Serializer):
    thumbnail = serializers.ImageField()


###################################################### USER
class UserIdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    email = serializers.EmailField()
    name = serializers.CharField()
    pseudo = serializers.CharField()
    abstract = serializers.CharField()
    title = serializers.CharField()
    image = serializers.ImageField()
    location = serializers.CharField()
    longitude = serializers.FloatField()
    latitude = serializers.FloatField()
    is_online = serializers.BooleanField()
    is_verified = serializers.BooleanField()


class UserIdShortSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    email = serializers.EmailField()
    name = serializers.CharField()
    pseudo = serializers.CharField()
    thumbnail = serializers.ImageField()
    is_online = serializers.BooleanField()
    is_verified = serializers.BooleanField()


###################################################### SKILL
class SkillIdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    body = serializers.CharField()


class SkillSerializer(serializers.Serializer):
    title = serializers.CharField()
    body = serializers.CharField()


###################################################### EXPERIENCE
class ExperienceIdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    body = serializers.CharField()
    begin = serializers.DateField()
    end = serializers.DateField()


class ExperienceSerializer(serializers.Serializer):
    title = serializers.CharField()
    body = serializers.CharField()
    begin = serializers.DateField()
    end = serializers.DateField(required=False)


###################################################### HASHTAG
class HashtagIdSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    tag = serializers.CharField()


class HashtagSerializer(serializers.Serializer):
    tag = serializers.CharField()


###################################################### PASSWORD
class CreateUserSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password_hash = serializers.CharField()
    super_password_hash = serializers.CharField()
    encrypted_user_key = serializers.CharField()
    encrypted_super_user_key = serializers.CharField()
    encrypted_private_key = serializers.CharField()
    public_key = serializers.CharField()
    location = serializers.CharField()
    longitude = serializers.FloatField()
    latitude = serializers.FloatField()


class LoginPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password_hash = serializers.CharField()


class LoginSuperPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()
    super_password_hash = serializers.CharField()
    new_password_hash = serializers.CharField()
    new_encrypted_user_key = serializers.CharField()


class PasswordSerializer(serializers.Serializer):
    password_hash = serializers.CharField()
    new_password_hash = serializers.CharField()
    new_encrypted_user_key = serializers.CharField()


class SuperPasswordSerializer(serializers.Serializer):
    password_hash = serializers.CharField()
    new_super_password_hash = serializers.CharField()
    new_encrypted_super_user_key = serializers.CharField()


class EmailPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password_hash = serializers.CharField()


class EmailSuperPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()
    super_password_hash = serializers.CharField()


class EncryptedSuperUserKeySerializer(serializers.Serializer):
    encrypted_super_user_key = serializers.CharField()