__author__ = 'pierrestarkov'
from rest_framework.parsers import JSONParser
from django.utils.six import BytesIO


def json_to_data(json):
    stream = BytesIO(json)
    return JSONParser().parse(stream)