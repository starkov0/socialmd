__author__ = 'pierrestarkov'
from django.contrib import auth


def get_user_in_request(request):
    """
    get user from request
    """
    if not hasattr(request, '_cached_user'):
        request._cached_user = auth.get_user(request)
    return request._cached_user