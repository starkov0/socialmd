__author__ = 'pierrestarkov'
from rest_framework import permissions
from socialApp.models.conversationuser import ConversationUser


class IsNotAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and not request.user.is_authenticated()
