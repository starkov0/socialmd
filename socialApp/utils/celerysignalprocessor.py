__author__ = 'pierrestarkov'
from celery_haystack.signals import CelerySignalProcessor
from socialApp.models.experience import Experience
from socialApp.models.skill import Skill
from socialApp.models.hashtag import Hashtag
from socialApp.models.relationship import Relationship
from socialApp.models.conversationuser import ConversationUser
from socialApp.models.user import User


class CelerySignalProcessorRelatedFields(CelerySignalProcessor):

    def enqueue_save(self, sender, instance, **kwargs):
        if isinstance(instance, Experience) or isinstance(instance, Skill) or isinstance(instance, Hashtag):
            self.enqueue('update', instance.user, sender, **kwargs)
        if isinstance(instance, Relationship):
            self.enqueue('update', instance.user_low, sender, **kwargs)
            self.enqueue('update', instance.user_high, sender, **kwargs)
        if isinstance(instance, ConversationUser):
            for conversationuser in instance.conversation.conversationuser_set.all():
                self.enqueue('update', conversationuser, sender, **kwargs)
                self.enqueue('update', conversationuser.user, sender, **kwargs)
        if isinstance(instance, User):
            for conversationuser in instance.conversationuser_set.all():
                self.enqueue('update', conversationuser, sender, **kwargs)
            for hashtag in instance.hashtag_set.all():
                self.enqueue('update', hashtag, sender, **kwargs)
        return self.enqueue('update', instance, sender, **kwargs)

    def enqueue_delete(self, sender, instance, **kwargs):
        if isinstance(instance, Experience) or isinstance(instance, Skill) or isinstance(instance, Hashtag):
            self.enqueue('update', instance.user, sender, **kwargs)
        if isinstance(instance, Relationship):
            self.enqueue('update', instance.user_low, sender, **kwargs)
            self.enqueue('update', instance.user_high, sender, **kwargs)
        return self.enqueue('delete', instance, sender, **kwargs)