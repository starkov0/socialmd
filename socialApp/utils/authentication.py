__author__ = 'pierrestarkov'
from socialApp.models.user import User


class BackendAuthentication(object):

    def authenticate(self, username, password):
        try:
            user = User.objects.get(email=username, is_active=True)
        except User.DoesNotExist:
            return None

        if user.check_password_hash(password_hash=password):
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None