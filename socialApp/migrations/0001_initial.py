# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import socialApp.models.messagefile
from django.utils.timezone import utc
import easy_thumbnails.fields
from django.conf import settings
import socialApp.models.user


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(unique=True, max_length=75)),
                ('name', models.TextField()),
                ('pseudo', models.TextField()),
                ('location', models.TextField()),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('title', models.TextField()),
                ('abstract', models.TextField()),
                ('image', models.ImageField(upload_to=socialApp.models.user.upload_image_handler)),
                ('thumbnail', easy_thumbnails.fields.ThumbnailerImageField(upload_to=socialApp.models.user.upload_thumbnail_handler)),
                ('is_active', models.BooleanField(default=True)),
                ('is_online', models.BooleanField(default=True)),
                ('is_verified', models.BooleanField(default=False)),
                ('last_login', models.DateTimeField(default=datetime.datetime(2015, 4, 9, 23, 19, 13, 765485, tzinfo=utc), auto_now=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime(2015, 4, 9, 23, 19, 13, 765542, tzinfo=utc), auto_now_add=True)),
                ('password_hash', models.TextField()),
                ('super_password_hash', models.TextField()),
                ('encrypted_user_key', models.TextField()),
                ('encrypted_super_user_key', models.TextField()),
                ('encrypted_private_key', models.TextField()),
                ('public_key', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Conversation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('encrypted_title', models.TextField()),
                ('created_at', models.DateTimeField(default=datetime.datetime(2015, 4, 9, 23, 19, 13, 417706, tzinfo=utc), auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConversationNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('conversation', models.ForeignKey(to='socialApp.Conversation')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ConversationUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('encrypted_conversation_key', models.TextField()),
                ('has_seen', models.BooleanField(default=False)),
                ('is_archived', models.BooleanField(default=False)),
                ('last_seen', models.DateTimeField(default=datetime.datetime(2015, 4, 9, 23, 19, 13, 440708, tzinfo=utc), auto_now=True)),
                ('conversation', models.ForeignKey(to='socialApp.Conversation')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Experience',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('body', models.TextField()),
                ('begin', models.DateField()),
                ('end', models.DateField(null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Hashtag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.TextField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime(2015, 4, 9, 23, 19, 13, 446695, tzinfo=utc), auto_now_add=True)),
                ('conversationuser', models.ForeignKey(to='socialApp.ConversationUser')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MessageFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('position', models.IntegerField()),
                ('encrypted_file', models.FileField(upload_to=socialApp.models.messagefile.upload_path_handler)),
                ('message', models.ForeignKey(to='socialApp.Message')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MessageNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('conversation', models.ForeignKey(to='socialApp.Conversation')),
                ('message', models.ForeignKey(to='socialApp.Message')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MessageText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('position', models.IntegerField()),
                ('encrypted_text', models.TextField()),
                ('message', models.ForeignKey(to='socialApp.Message')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('has_seen', models.BooleanField(default=False)),
                ('has_checked', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(default=datetime.datetime(2015, 4, 9, 23, 19, 13, 453532, tzinfo=utc), auto_now_add=True)),
                ('user_notified', models.ForeignKey(related_name='notification_notified', to=settings.AUTH_USER_MODEL)),
                ('user_notifying', models.ForeignKey(related_name='notification_notifying', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.TextField(default=b'is_waiting', choices=[(b'is_waiting', b'IS_WAITING'), (b'is_accepted', b'IS_ACCEPTED')])),
                ('user_answering', models.ForeignKey(related_name='relationship_answering', to=settings.AUTH_USER_MODEL)),
                ('user_asking', models.ForeignKey(related_name='relationship_asking', to=settings.AUTH_USER_MODEL)),
                ('user_high', models.ForeignKey(related_name='relationship_high', to=settings.AUTH_USER_MODEL)),
                ('user_low', models.ForeignKey(related_name='relationship_low', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RelationshipNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.TextField(default=b'is_waiting', choices=[(b'is_waiting', b'IS_WAITING'), (b'is_accepted', b'IS_ACCEPTED')])),
                ('notification', models.OneToOneField(to='socialApp.Notification')),
                ('relationship', models.ForeignKey(to='socialApp.Relationship')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('body', models.TextField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='relationship',
            unique_together=set([('user_high', 'user_low')]),
        ),
        migrations.AddField(
            model_name='messagenotification',
            name='notification',
            field=models.OneToOneField(to='socialApp.Notification'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='conversationuser',
            unique_together=set([('user', 'conversation')]),
        ),
        migrations.AddField(
            model_name='conversationnotification',
            name='notification',
            field=models.OneToOneField(to='socialApp.Notification'),
            preserve_default=True,
        ),
    ]
