#Technical architecture

Au début, architecture simple 1-tier: un 'simple' serveur avec:
1. Front-end web (apache)
2. BDD (MariaDB)
3. search engine (elastic)
3. serveur email


Plus tard, on peut 2-tier (BDD séparée), en encore plus tard séparer le content 
statique (aka l'application mobile/web/…) de l'applicatif dur (app REST). Mais 
ya encore largement le temps.


#Continous integration
A voir si on utilise deux/trois outils:
1. Jenkins
2. Sonar ?
