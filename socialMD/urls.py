from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
#
from socialApp.views.user.io.encrypted_super_user_key_view import encrypted_super_user_key_view
from socialApp.views.user.io.user_create_view import user_create_view
from socialApp.views.user.io.activate_password_hash_view import activate_password_hash_view
from socialApp.views.user.io.activate_super_password_hash_view import activate_super_password_hash_view
from socialApp.views.user.io.login_password_hash_view import login_password_hash_view
from socialApp.views.user.io.login_super_password_hash_view import login_super_password_hash_view
from socialApp.views.user.io.deactivate_view import deactivate_view
from socialApp.views.user.io.logout_view import logout_view
#
from socialApp.views.user.information.abstract_view import abstract_view
from socialApp.views.user.information.email_view import email_view
from socialApp.views.user.information.location_view import location_view
from socialApp.views.user.information.name_view import name_view
from socialApp.views.user.information.image_view import image_view
from socialApp.views.user.information.thumbnail_view import thumbnail_view
from socialApp.views.user.information.title_view import title_view
from socialApp.views.user.information.password_hash_view import password_hash_view
from socialApp.views.user.information.super_password_hash_view import super_password_hash_view
from socialApp.views.user.information.user_view import user_view
#
from socialApp.views.user.experience.experience_count_view import experience_count_view
from socialApp.views.user.experience.experience_id_view import experience_id_view
from socialApp.views.user.experience.experience_create_view import experience_create_view
from socialApp.views.user.experience.experience_search_view import experience_search_view
#
from socialApp.views.user.skill.skill_count_view import skill_count_view
from socialApp.views.user.skill.skill_create_view import skill_create_view
from socialApp.views.user.skill.skill_id_view import skill_id_view
from socialApp.views.user.skill.skill_search_view import skill_search_view
#
from socialApp.views.user.hashtag.hashtag_count_view import hashtag_count_view
from socialApp.views.user.hashtag.hashtag_create_view import hashtag_create_view
from socialApp.views.user.hashtag.hashtag_id_view import hashtag_id_view
from socialApp.views.user.hashtag.hashtag_search_view import hashtag_search_view
#
from socialApp.views.user.relationship.relationship_count_view import relationship_count_view
from socialApp.views.user.relationship.relationship_id_accept_view import relationship_id_accept_view
from socialApp.views.user.relationship.relationship_id_create_view import relationship_id_create_view
from socialApp.views.user.relationship.relationship_id_view import relationship_id_view
from socialApp.views.user.relationship.relationship_search_view import relationship_search_view
#
from socialApp.views.public.experience.user_id_experience_count_view import user_id_experience_count_view
from socialApp.views.public.experience.user_id_experience_id_view import user_id_experience_id_view
from socialApp.views.public.experience.user_id_experience_search_view import user_id_experience_search_view
#
from socialApp.views.public.hashtag.user_id_hashtag_count_view import user_id_hashtag_count_view
from socialApp.views.public.hashtag.user_id_hashtag_id_view import user_id_hashtag_id_view
from socialApp.views.public.hashtag.user_id_hashtag_search_view import user_id_hashtag_search_view
#
from socialApp.views.public.hashtag.hashtag_only_search_view import hashtag_only_search_view
#
from socialApp.views.public.skill.user_id_skill_cout_view import user_id_skill_count_view
from socialApp.views.public.skill.user_id_skill_id_view import user_id_skill_id_view
from socialApp.views.public.skill.user_id_skill_search_view import user_id_skill_search_view
#
from socialApp.views.public.user.user_id_view import user_id_view
from socialApp.views.public.user.user_search_view import user_search_view
#
from socialApp.views.public.relationship.user_id_relationship_count_view import user_id_relationship_count_view
from socialApp.views.public.relationship.user_id_relationship_search_view import user_id_relationship_search_view
#
from socialApp.views.public.public_key.user_id_public_key_view import user_id_public_key_view
#
from socialApp.views.conversation.io.conversation_count_view import conversation_count_view
from socialApp.views.conversation.io.conversation_create_view import conversation_create_view
from socialApp.views.conversation.io.conversation_search_view import conversation_search_view
#
from socialApp.views.conversation.conversation_id.conversation_id_archive_view import conversation_id_archive_view
from socialApp.views.conversation.conversation_id.conversation_id_conversation_view import conversation_id_conversation_view
from socialApp.views.conversation.conversation_id.conversation_id_encrypted_conversation_key_view import conversation_id_encrypted_conversation_key_view
from socialApp.views.conversation.conversation_id.conversation_id_encrypted_title_view import conversation_id_encrypted_title_view
#
from socialApp.views.conversation.conversation_id_user.conversation_id_user_add_view import conversation_id_user_add_view
from socialApp.views.conversation.conversation_id_user.conversation_id_user_count_view import conversation_id_user_count_view
from socialApp.views.conversation.conversation_id_user.conversation_id_user_id_conversationuser_view import conversation_id_user_id_conversationuser_view
from socialApp.views.conversation.conversation_id_user.conversation_id_user_search_view import conversation_id_user_search_view
from socialApp.views.conversation.conversation_id_user.conversation_id_user_to_add_count_view import conversation_id_user_to_add_count_view
from socialApp.views.conversation.conversation_id_user.conversation_id_user_to_add_search_view import conversation_id_user_to_add_search_view
#
from socialApp.views.conversation.message.message_count_view import message_count_view
from socialApp.views.conversation.message.message_create_view import message_create_view
from socialApp.views.conversation.message.message_id_view import message_id_view
from socialApp.views.conversation.message.message_search_view import message_search_view
from socialApp.views.conversation.message.messagefile_id_view import messagefile_id_view

urlpatterns = [

    # ACCESSIBLE TO OWNER OF THE USER ACCOUNT
    url(r'^user/create/$', user_create_view),
    url(r'^activate/password_hash/$', activate_password_hash_view),
    url(r'^activate/super_password_hash/$', activate_super_password_hash_view),
    url(r'^deactivate/$', deactivate_view),
    url(r'^login/password_hash/$', login_password_hash_view),
    url(r'^login/super_password_hash/$', login_super_password_hash_view),
    url(r'^logout/$', logout_view),

    url(r'^encrypted_super_user_key/$', encrypted_super_user_key_view),

    url(r'^abstract/$', abstract_view),
    url(r'^email/$', email_view),
    url(r'^location/$', location_view),
    url(r'^name/$', name_view),
    url(r'^title/$', title_view),
    url(r'^password_hash/$', password_hash_view),
    url(r'^super_password_hash/$', super_password_hash_view),
    url(r'^image/$', image_view),
    url(r'^thumbnail/$', thumbnail_view),
    url(r'^user/$', user_view),

    url(r'^experience/count/$', experience_count_view),
    url(r'^experience/create/$', experience_create_view),
    url(r'^experience'
        r'/experience_id=(?P<experience_id>[\d]+)/$', experience_id_view),
    url(r'^experience/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', experience_search_view),

    url(r'^skill/count/$', skill_count_view),
    url(r'^skill/create/$', skill_create_view),
    url(r'^skill'
        r'/skill_id=(?P<skill_id>[\d]+)/$', skill_id_view),
    url(r'^skill/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', skill_search_view),

    url(r'^hashtag/count/$', hashtag_count_view),
    url(r'^hashtag/create/$', hashtag_create_view),
    url(r'^hashtag'
        r'/hashtag_id=(?P<hashtag_id>[\d]+)/$', hashtag_id_view),
    url(r'^hashtag/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', hashtag_search_view),

    # ACCESSIBLE TO LOGGED IN USERS
    url(r'^relationship/count'
        r'/state=(?P<state>[\w]+)/$', relationship_count_view),
    url(r'^relationship/create'
        r'/user_id=(?P<user_id>[\d]+)/$', relationship_id_create_view),
    url(r'^relationship/accept'
        r'/user_id=(?P<user_id>[\d]+)/$', relationship_id_accept_view),
    url(r'^relationship'
        r'/user_id=(?P<user_id>[\d]+)/$', relationship_id_view),
    url(r'^relationship/search'
        r'/state=(?P<state>[\w]+)'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/longitude=(?P<longitude>[\d.\d]*)'
        r'/latitude=(?P<latitude>[\d.\d]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', relationship_search_view),

    # ACCESSIBLE TO EVERYBODY
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/experience/count/$', user_id_experience_count_view),
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/experience'
        r'/experience_id=(?P<experience_id>[\d]+)/$', user_id_experience_id_view),
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/experience/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', user_id_experience_search_view),

    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/hashtag/count/$', user_id_hashtag_count_view),
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/hashtag'
        r'/hashtag_id=(?P<hashtag_id>[\d]+)/$', user_id_hashtag_id_view),
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/hashtag/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', user_id_hashtag_search_view),

    url(r'^hashtag_only/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', hashtag_only_search_view),

    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/skill/count/$', user_id_skill_count_view),
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/skill'
        r'/skill_id=(?P<skill_id>[\d]+)/$', user_id_skill_id_view),
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/skill/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', user_id_skill_search_view),

    url(r'^user_id=(?P<user_id>[\d]+)/user/$', user_id_view),
    url(r'^user/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/longitude=(?P<longitude>[\d.\d]*)'
        r'/latitude=(?P<latitude>[\d.\d]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', user_search_view),

    # ACCESSIBLE TO LOGGED IN USERS
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/relationship/count'
        r'/is_mutual=(?P<is_mutual>[\w,]+)/$', user_id_relationship_count_view),
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/relationship/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/longitude=(?P<longitude>[\d.\d]*)'
        r'/latitude=(?P<latitude>[\d.\d]*)'
        r'/is_mutual=(?P<is_mutual>[\w,]+)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', user_id_relationship_search_view),

    # ACCESSIBLE TO USERS HAVING AN ACTIVE RELATIONSHIP WITH THE USER
    url(r'^user_id=(?P<user_id>[\d]+)'
        r'/public_key/$', user_id_public_key_view),

    # ACCESSIBLE TO OWNER OF THE USER ACCOUNT
    url(r'^conversation/count'
        r'/is_archived=(?P<is_archived>[\w,]+)/$', conversation_count_view),
    url(r'^conversation/create/$', conversation_create_view),
    url(r'^conversation/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/is_archived=(?P<is_archived>[\w,]+)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', conversation_search_view),

    # ACCESSIBLE TO OWNER OF THE USER ACCOUNT
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/archive/$', conversation_id_archive_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/conversation/$', conversation_id_conversation_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/encrypted_title/$', conversation_id_encrypted_title_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/encrypted_conversation_key/$', conversation_id_encrypted_conversation_key_view),

    # ACCESSIBLE TO USERS BEING IN THE CONVERSATION
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/user_id=(?P<user_id>[\d]+)'
        r'/add/$', conversation_id_user_add_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/user_id=(?P<user_id>[\d]+)'
        r'/conversationuser/$', conversation_id_user_id_conversationuser_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/user/count/$', conversation_id_user_count_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/user/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', conversation_id_user_search_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/user_to_add/count/$', conversation_id_user_to_add_count_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/user_to_add/search'
        r'/keywords=(?P<keywords>[\w,]*)'
        r'/index_from=(?P<index_from>[\d]+)'
        r'/index_to=(?P<index_to>[\d]+)/$', conversation_id_user_to_add_search_view),

    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/message/count/$', message_count_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/message/create/$', message_create_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/message/search/$', message_search_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/message_id=(?P<message_id>[\d]+)/$', message_id_view),
    url(r'^conversation_id=(?P<conversation_id>[\d]+)'
        r'/messagefile_id=(?P<messagefile_id>[\d]+)/$', messagefile_id_view),

]

urlpatterns = format_suffix_patterns(urlpatterns)