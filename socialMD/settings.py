from __future__ import absolute_import
import os
import warnings

warnings.filterwarnings(
        'error', r"DateTimeField .* received a naive datetime",
        RuntimeWarning, r'django\.db\.models\.fields')

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admindocs',
    'django_extensions',
    'kombu.transport.django',
    'celery_haystack',
    'rest_framework',
    'haystack',
    'easy_thumbnails',
    'socialApp',
)

BROKER_URL = 'redis://localhost:6379/1'
BROKER_TRANSPORT = 'redis'
BROKER_BACKEND = 'memory'
BROKER_USE_SSL = True

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'
CELERY_IGNORE_RESULT = False
CELERY_MESSAGE_COMPRESSION = 'gzip'

TEST_RUNNER = 'djcelery.contrib.test_runner.CeleryTestSuiteRunner'

AUTH_USER_MODEL = 'socialApp.User'

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'socialApp.utils.authentication.BackendAuthentication',
)

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

ROOT_URLCONF = 'socialMD.urls'

WSGI_APPLICATION = 'socialMD.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'mydatabase',
    }
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'haystack',
    },
}

HAYSTACK_SIGNAL_PROCESSOR = 'socialApp.utils.celerysignalprocessor.CelerySignalProcessorRelatedFields'
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'socialMDdb',
#         'USER': 'pierrestarkov',
#         'PASSWORD': '',
#         'HOST': '127.0.0.1',
#         'PORT': '5432',
#     }
# }

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}


THUMBNAIL_ALIASES = {
    '': {
        'avatar': {'size': (50, 50), 'crop': True},
    },
}

SECRET_KEY = '#+bxv@@tful1_5=ie&!^e93kl+=i%715hz0tw)^%73woijq$wk'

DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = []
LANGUAGE_CODE = 'en-us'
# CELERY_TIMEZONE = 'UTC'
# CELERY_ENABLE_UTC = True
USE_I18N = True
USE_L10N = True
USE_TZ = True
FILE_UPLOAD_PERMISSIONS = 0600
APPEND_SLASH = False

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
MEDIA_URL = '/media/'
MEDIA_ROOT = PROJECT_DIR + MEDIA_URL
STATIC_URL = '/static/'
STATIC_ROOT = PROJECT_DIR + STATIC_URL
TEMPLATES_URL = '/templates/'
TEMPLATE_DIRS = (PROJECT_DIR + TEMPLATES_URL,)

# TEMPLATE_LOADERS = (
#     'django.templates.loaders.filesystem.load_template_source',
#     'django.templates.loaders.app_directories.load_template_source',
# #     'django.templates.loaders.eggs.load_template_source',
# )